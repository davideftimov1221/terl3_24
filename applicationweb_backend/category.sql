DELETE FROM category;
-- ELECTRONICS
INSERT INTO category (id, name, parent_category)
VALUES ('1', 'Electronics', NULL);
INSERT INTO category (id, name, parent_category)
VALUES ('8', 'CameraPhoto', '1');
INSERT INTO category (id, name, parent_category)
VALUES ('9', 'Headphones', '1');
INSERT INTO category (id, name, parent_category)
VALUES (
        '10',
        'ComputerAccessories',
        '1'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '11',
        'AccessoriesSupplies',
        '1'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '12',
        'CarVehicleElectronics',
        '1'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '13',
        'CellPhonesAccessories',
        '1'
    );
-- COMPUTERS
INSERT INTO category (id, name, parent_category)
VALUES ('2', 'Computers', NULL);
INSERT INTO category (id, name, parent_category)
VALUES (
        '14',
        'GraphiqueCards',
        '2'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '15',
        'Processor',
        '2'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '16',
        'MotherBoard',
        '2'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '17',
        'PowerSupply',
        '2'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '18',
        'ComputerTower',
        '2'
    );
INSERT INTO category (id, name, parent_category)
VALUES ('19', 'Fan', '2');
-- WOMENSFASHION
INSERT INTO category (id, name, parent_category)
VALUES (
        '3',
        'WomensFashion',
        NULL
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '20',
        'WomensShoes',
        '3'
    );
INSERT INTO category (id, name, parent_category)
VALUES ('21', 'Dress', '3');
INSERT INTO category (id, name, parent_category)
VALUES (
        '22',
        'WomensUnderwear',
        '3'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '23',
        'WomensJeans',
        '3'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '24',
        'WomensTshirt',
        '3'
    );
INSERT INTO category (id, name, parent_category)
VALUES ('25', 'WomensHat', '3');
-- MENSFASHION
INSERT INTO category (id, name, parent_category)
VALUES (
        '4',
        'MensFashion',
        NULL
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '26',
        'MensShoes',
        '4'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '27',
        'MensUnderwear',
        '4'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '28',
        'MensJeans',
        '4'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '29',
        'MensTshirt',
        '4'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '30',
        'MensHat',
        '4'
    );
INSERT INTO category (id, name, parent_category)
VALUES ('31', 'Sunglasses', '4');
-- GIRLSFASHION
INSERT INTO category (id, name, parent_category)
VALUES (
        '5',
        'GirlsFashion',
        NULL
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '32',
        'GirlsShoes',
        '5'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '33',
        'GirlsUnderwear',
        '5'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '34',
        'GirlsJeans',
        '5'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '35',
        'GirlsTshirt',
        '5'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '36',
        'GirlsHat',
        '5'
    );
INSERT INTO category (id, name, parent_category)
VALUES ('37', 'GirlsDisguise', '5');
-- BOYSFASHION
INSERT INTO category (id, name, parent_category)
VALUES (
        '6',
        'BoysFashion',
        NULL
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '38',
        'BoysShoes',
        '6'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '39',
        'BoysUnderwear',
        '6'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '40',
        'BoysJeans',
        '6'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '41',
        'BoysTshirt',
        '6'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '42',
        'BoysHat',
        '6'
    );
INSERT INTO category (id, name, parent_category)
VALUES ('43', 'BoysDisguise', '6');
-- SPORTANDOUTDOORS
INSERT INTO category (id, name, parent_category)
VALUES (
        '7',
        'SportandOutdoors',
        NULL
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '44',
        'Climbing',
        '7'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '45',
        'Trekking',
        '7'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '46',
        'Swimming',
        '7'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '47',
        'MountainBike',
        '7'
    );
INSERT INTO category (id, name, parent_category)
VALUES (
        '48',
        'Canyoning',
        '7'
    );
INSERT INTO category (id, name, parent_category)
VALUES ('49', 'Diving', '7');