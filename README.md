# E-commerce Web Application for Local Businesses and Customers


This project is a web application that allows local businesses to sell their products online and customers to buy them easily. It is based on an existing [mobile application](https://github.com/Abouelyatim/ShopsServicesProximity_Mobile_Client) developed by BERKANE Ibrahim. The web application is constructed using React for the frontend and Spring Boot for the backend (developed by Ibrahim and adapted by us).

## Features

The web application has distinct features for two user roles: merchants and customers.

- For merchants:
  - Create and edit their store profile with description, photo, address, and activity domain.
  - Manage their products by adding categories, products, images, prices, and quantities.
  - Create and manage special offers such as flash sales or discounts.
  - View and handle orders from customers by accepting or rejecting them, and updating their status.
- For customers:
  - Configure their preferences such as interests and location.
  - Search for products by name, category or distance.
  - View product details and store information.
  - Add products to their cart and place orders with different delivery options.
  - Manage their cart and orders.

## Authors

- Guillaume Hakenholz
- David Eftimov

## Demo

[Video demonstration](https://drive.google.com/file/d/1aARCT43ehAwuq_L8QWXq4zrslqy1TG9i/view)







