import ColCategory from "./ColCategory";

export default function RowCategory({
  categories,
  numeroRow,
  nbCategoriesPerRow,
  categoriesChoose,
  setCategoriesChoose,
  setCheckSubCategories,
}) {
  function colCategory(categories) {
    let elements = [];
    const index = nbCategoriesPerRow * numeroRow;
    for (let i = 0; i < nbCategoriesPerRow; i++) {
      categories &&
        categories[index + i] &&
        elements.push(
          <ColCategory
            category={categories[index + i]}
            categoriesChoose={categoriesChoose}
            setCategoriesChoose={setCategoriesChoose}
            setCheckSubCategories={setCheckSubCategories}
            key={index + i}
          />
        );
    }
    return elements;
  }
  return <div className="row activityfield-row">{colCategory(categories)}</div>;
}
