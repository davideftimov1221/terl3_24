import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function ColCategory({
  category,
  categoriesChoose,
  setCategoriesChoose,
  setCheckSubCategories,
}) {
  const [checked, setChecked] = useState(false);

  const navigate = useNavigate();

  function handleClick(event) {
    const categorieschoose = categoriesChoose.slice();

    const indexCategory = categorieschoose.findIndex(
      (element) => element.name === category.name
    );
    if (indexCategory !== -1) {
      categorieschoose.splice(indexCategory, 1);
      setCategoriesChoose(categorieschoose);
      setChecked(false);
    } else {
      categorieschoose.push({
        id: category.id,
        name: category.name,
        subCategoriesChoose: [],
      });
      setCategoriesChoose(categorieschoose);
      setChecked(true);

      if (category.subCategorys.length !== 0) {
        navigate(`subcategory/${category.id}`);
        setCheckSubCategories(false);
      }
    }
  }

  useEffect(
    () =>
      categoriesChoose.findIndex(
        (element) => element.name === category.name
      ) !== -1
        ? setChecked(true)
        : setChecked(false),
    []
  );
  return (
    <div className="col p-0 d-flex justify-content-center">
      <label>
        <input
          className="checkbox btn-check"
          type="checkbox"
          checked={checked}
          onChange={() => {}}
        />
        <button
          className="btn btn-outline-dark btn-lg btn-block"
          onClick={handleClick}
        >
          {category.name}
        </button>
      </label>
    </div>
  );
}
