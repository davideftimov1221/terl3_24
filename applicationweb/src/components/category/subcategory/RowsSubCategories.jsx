import RowSubCategory from "./RowSubCategory";

export default function RowsSubCategories({
  subcategories,
  nbSubCategoriesPerRow,
  subCategories,
  setCategoriesChoose,
}) {
  function rows() {
    const elements = [];
    for (let i = 0; i < subcategories.length / nbSubCategoriesPerRow; i++) {
      elements.push(
        <RowSubCategory
          subcategories={subcategories}
          numeroRow={i}
          nbSubCategoriesPerRow={nbSubCategoriesPerRow}
          subCategories={subCategories}
          setCategoriesChoose={setCategoriesChoose}
          key={i}
        />
      );
    }

    return elements;
  }
  return <div>{rows()}</div>;
}
