import ColSubCategory from "./ColSubCategory";

export default function RowSubCategory({
  subcategories,
  numeroRow,
  nbSubCategoriesPerRow,
  subCategories,
  setCategoriesChoose,
}) {
  function colCategory(subcategories) {
    let elements = [];
    const index = nbSubCategoriesPerRow * numeroRow;
    for (let i = 0; i < nbSubCategoriesPerRow; i++) {
      subcategories[index + i] &&
        elements.push(
          <ColSubCategory
            subCategory={subcategories[index + i]}
            subCategories={subCategories}
            setCategoriesChoose={setCategoriesChoose}
            key={i}
          />
        );
    }
    return elements;
  }
  return (
    <div className="row activityfield-row">{colCategory(subcategories)}</div>
  );
}
