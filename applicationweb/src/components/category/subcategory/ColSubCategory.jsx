import { useEffect, useState } from "react";

export default function ColSubCategory({
  subCategory,
  subCategories,
  setCategoriesChoose,
}) {
  const [checked, setChecked] = useState(false);

  function handleClick(event) {
    const subcategories = subCategories.slice();

    if (checked) {
      const indexCategory = subcategories.findIndex(
        (element) => element === subCategory
      );
      subcategories.splice(indexCategory, 1);
      setChecked(false);
    } else {
      subcategories.push(subCategory);
      setChecked(true);
    }
    setCategoriesChoose(subcategories);
  }

  useEffect(() => {
    subCategories.includes(subCategory) ? setChecked(true) : setChecked(false);
  }, []);

  return (
    <div className="col p-0 d-flex justify-content-center">
      <input
        className="checkbox btn-check"
        onClick={handleClick}
        valeur={subCategory}
        type="checkbox"
        id={subCategory}
        checked={checked}
        onChange={() => {}}
      />
      <label
        className="btn btn-outline-dark btn-lg btn-block"
        htmlFor={subCategory}
      >
        {subCategory}
      </label>
    </div>
  );
}
