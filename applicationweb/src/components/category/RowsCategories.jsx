import RowCategory from "./RowCategory";

export default function RowsCategories({
  categories,
  nbCategoriesPerRow,
  categoriesChoose,
  setCategoriesChoose,
  setCheckSubCategories,
}) {
  function rows() {
    const elements = [];
    if (categories) {
      for (let i = 0; i < categories.length / nbCategoriesPerRow; i++) {
        elements.push(
          <RowCategory
            categories={categories}
            numeroRow={i}
            nbCategoriesPerRow={nbCategoriesPerRow}
            categoriesChoose={categoriesChoose}
            setCategoriesChoose={setCategoriesChoose}
            setCheckSubCategories={setCheckSubCategories}
            key={categories[i].id}
          />
        );
      }
    }
    return elements;
  }
  return <div>{rows()}</div>;
}
