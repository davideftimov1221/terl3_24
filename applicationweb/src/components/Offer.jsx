import React from "react";

export default function Offer(props) {
  function click() {
    props.deleteOffer(props.id);
  }
  return (
    <div className="col-3">
      <h6>{props.name}</h6>
      <p className="m-0">Produits: </p>
      <div className="row mb-2">
        {props.products.map((product) => {
          return (
            <div key={product.id} className="col-6">
              {product.name}
            </div>
          );
        })}
      </div>
      <button className="btn btn-outline-dark mb-3" onClick={click}>
        Delete
      </button>
    </div>
  );
}
