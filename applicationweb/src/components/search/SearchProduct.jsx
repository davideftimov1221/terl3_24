import { faCartPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useContext, useEffect, useState } from "react";
import "../../styles/home2.css";
import { Link, Navigate, useNavigate } from "react-router-dom";
import ProductContext from "../../context/ProductProvider";

export default function SearchProduct({ Product }) {
  const { product, setProduct } = useContext(ProductContext);
  const [productItem, setProductItem] = useState(Product);
  const [customCategory, setCustomCategory] = useState();

  const navigate = useNavigate();

  function goToProduct() {
    setProduct(productItem);
    navigate("../../../product");
  }
  //console.log(productItem.productVariants[0].offer);
  useEffect(() => {}, [productItem]);

  function calculatePrice() {
    if (productItem.productVariants[0].offer.percentage === 0) {
      return productItem.productVariants[0].offer.newPrice;
    } else {
      return (
        productItem.productVariants[0].price *
        ((100 - productItem.productVariants[0].offer.percentage) / 100)
      );
    }
  }

  return (
    <div className="col-6 col-md-4 col-lg-3 mb-3">
      <div className="card productCard">
        <div className="card-header bg-transparent border-0">
          <div className="row d-flex">
            <div className="col-10 col-lg-9 col-xl-10">
              <h5>{productItem.name}</h5>
            </div>
            <div className="col-2 col-lg-3 col-xl-2">
              <div className="text-center">
                {productItem.productVariants[0].offer !== null && (
                  <div className="card--badge ms-5 ms-lg-2">SALE</div>
                )}
              </div>
            </div>
          </div>
          <div className="d-flex">
            {productItem.productVariants[0].offer !== null && (
              <p className="oldPrice m-0">-----</p>
            )}
            <p className="m-0">€{productItem.productVariants[0].price}</p>
            {productItem.productVariants[0].offer !== null && (
              <p className="m-0 ms-1">€{calculatePrice()}</p>
            )}
          </div>
        </div>
        <img
          src={
            "http://localhost:6969/api/product/image/" +
            productItem.images[0].image
          }
          alt=""
          className="card-img-top productCardImg"
        />

        <div className="card-footer bg-transparent border-0 text-center">
          <div className="row">
            <div className="col-5"></div>
            <div className="col-2 toCartIcon d-flex justify-content-center">
              <FontAwesomeIcon
                icon={faCartPlus}
                size="xl"
                onClick={goToProduct}
              />
            </div>
            <div className="col-5"></div>
          </div>
        </div>
      </div>
    </div>
  );
}
