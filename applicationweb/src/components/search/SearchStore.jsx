import React, { useEffect, useState } from "react";
import axios from "../../api/axios";
import SearchProduct from "./SearchProduct";

export default function SearchStore(props) {
  const [productList, setProductList] = useState();
  useEffect(() => {
    const getStoreProducts = async () => {
      const response = await axios.get(
        `http://localhost:6969/api/product/all/store/${props.id}`
      );
      //console.log(response.data.result);
      setProductList(response.data.result);
    };
    getStoreProducts();
  }, []);

  function rows2() {
    let product;
    if (productList && productList !== []) {
      product = productList
        .filter((product) => {
          if (props.searchText === "") {
            return product;
          } else if (
            product.name.toLowerCase().includes(props.searchText.toLowerCase())
          ) {
            return product;
          }
        })
        .map((product) => {
          return <SearchProduct key={product.id} Product={product} />;
        });
    }
    return product;
  }
  return rows2();
}
