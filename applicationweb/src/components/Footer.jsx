import React, { useState } from "react";
import "../styles/footer.css";

export default function Footer() {
  const [newsEmail, setNewsEmail] = useState("");

  function handleChange(event) {
    setNewsEmail(event.target.value);
  }

  function handleSumbit(event) {
    event.preventDefault();
    //console.log(newsEmail)
  }

  return (
    <>
      <footer id="footerid">
        <div className="container-fluid">
          <div className="row d-flex justify-content-between">
            <div className="col-6">
              <h3>Support</h3>
              <p>Fags</p>
              <p>Suivre votre commande</p>
              <p>Contact</p>
            </div>
            <div className="col-6 col-lg-5">
              <h6>INSCRIVEZ-VOUS À NOTRE NEWSLETTER</h6>
              <div className="col-12 col-md-9 col-lg-7">
                <form
                  onSubmit={handleSumbit}
                  className="input-group"
                  id="footer-form"
                >
                  <input
                    type="text"
                    placeholder="Email"
                    onChange={handleChange}
                    name="email"
                    value={newsEmail}
                    id="footer-form-input"
                    className="form-control"
                  />
                  <button
                    className="btn btn-outline-dark mx-2"
                    id="footer-form-btn"
                  >
                    {"->"}
                  </button>
                </form>
              </div>
            </div>
          </div>
          <hr />
          <div className="row ">
            <div className="col-10">
              <p>©2022 ALL RIGHTS RESERVED</p>
            </div>
            <div className="col-2 d-flex justify-content-end">
              <p>FRANÇAIS</p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}
