import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencil, faTrash } from "@fortawesome/free-solid-svg-icons";
import "../styles/CustomCategory.css";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";

export default function AddCustomCategory({
  auth,
  customcategory,
  setCategory,
}) {
  const [customCategory, setCustomCategory] = useState({
    id: customcategory.id,
    userId: customcategory.name,
  });
  const navigate = useNavigate();

  async function handleClick() {
    navigate("../../myproducts");
  }

  useEffect(() => {
    const name = customcategory.name;
    setCustomCategory((prevData) => ({ ...prevData, name: name }));
  }, [customcategory]);
  return (
    <div
      key={customCategory.id}
      className="d-flex  px-3 mt-2 border-secondary border-top border-bottom justify-content-between "
    >
      <h4 className="my-2 w-100 customcategory-pointer" onClick={handleClick}>
        {customCategory.name}
      </h4>
      <div className="d-flex align-items-center justify-content-between  customcategory-icons">
        <FontAwesomeIcon
          className="customcategory-pointer"
          onClick={() => {
            setCategory((prevData) => ({
              ...prevData,
              id: customCategory.id,
              name: customCategory.name,
              provider: auth.id,
              valid: "update",
            }));
          }}
          icon={faPencil}
        />
        <FontAwesomeIcon
          className="customcategory-pointer"
          onClick={() => {
            setCategory((prevData) => ({
              ...prevData,
              id: customCategory.id,
              name: "",
              provider: auth.id,
              valid: "delete",
            }));
          }}
          icon={faTrash}
        />
      </div>
    </div>
  );
}
