import { faCartPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import "../styles/Cart.css";
import axios from "axios";
import photo from "../assets/productImg.jpg";
import CartProduct from "../components/CartProduct";
import { Link } from "react-router-dom";

export default function CartStore({
  name,
  cartInfo,
  authId,
  deleteAlert,
  setDeleteAlert,
}) {
  const [storeId, setStoreId] = useState();

  function findMyProducts() {
    let products = [];
    if (cartInfo) {
      for (let i = 0; i < cartInfo.length; i++) {
        if (name === cartInfo[i].storeName) {
          products.push(cartInfo[i]);
          // setStoreId(cartInfo[i].storeId);
        }
      }

      let productsMap = [];
      for (let i = 0; i < products.length; i++) {
        //console.log(products[i]);
        const key = products[i].id.cartProductVariantId;
        //console.log(key);
        productsMap.push(
          <CartProduct
            product={products[i]}
            authId={authId}
            deleteAlert={deleteAlert}
            setDeleteAlert={setDeleteAlert}
            key={key}
          />
        );
      }

      return productsMap;
    }
  }
  function findTotal() {
    let products = [];
    let total = 0;
    if (cartInfo) {
      for (let i = 0; i < cartInfo.length; i++) {
        if (name === cartInfo[i].storeName) {
          products.push(cartInfo[i]);
        }
      }

      for (let i = 0; i < products.length; i++) {
        if (products[i].productVariant.offer === null) {
          total = total + products[i].unit * products[i].productVariant.price;
        } else {
          if (products[i].productVariant.offer.percentage === 0) {
            total =
              total +
              products[i].unit * products[i].productVariant.offer.newPrice;
          } else {
            total =
              total +
              products[i].unit *
                (products[i].productVariant.price *
                  ((100 - products[i].productVariant.offer.percentage) / 100));
          }
        }
      }
    }
    return roundToTwo(total);
  }

  function roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2");
  }

  useEffect(() => {
    if (cartInfo) {
      const index = cartInfo.findIndex((element) => name === element.storeName);
      const storeid = cartInfo[index].storeId;
      setStoreId(storeid);
      //console.log(storeid);
    }
  }, []);
  return (
    <div>
      <div className="cartStoreName mb-3">
        <h5 className="m-0">{name}</h5>
      </div>
      <div className="row">
        <div className="col-7">Article</div>
        <div className="col-2">Quantité</div>
        <div className="col-2">Total</div>
        <div className="col-1"></div>
      </div>
      <hr />
      {findMyProducts()}
      <div className="row mb-5">
        <div className="col-6"></div>
        <div className="col-6">
          <hr />
          <div className="row">
            <div className="col-10">
              <h6 className="cartTOTAL">TOTAL:</h6>
            </div>
            <div className="col-2 text-end">
              <h6 className="cartTOTAL">{findTotal()}</h6>
            </div>
          </div>
          <hr />
          <div className="d-flex justify-content-center">
            <Link to={`../order/${storeId}`} className="btn btn-dark">
              PASSER LA COMMANDE
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
