import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { useEffect, useState } from "react";
import "../styles/Cart.css";
import axios from "axios";
import photo from "../assets/productImg.jpg";

export default function CartProduct({
  product,
  authId,
  deleteAlert,
  setDeleteAlert,
}) {
  const deleteProductFromCart = () => {
    axios
      .delete(
        `http://localhost:6969/api/cart/delete?userId=${authId}&variantId=${product.productVariant.id}`
      )
      .then(() => {
        alert("Product deleted!");
        setDeleteAlert(!deleteAlert);
      });
  };

  function calculatePrice () {
    if(product.productVariant.offer.percentage === 0){
      return product.productVariant.offer.newPrice
    } else {
      return product.productVariant.price * ((100 - product.productVariant.offer.percentage)/100)
    }
  }

  //console.log(product.productVariant.id);
  return (
    <div>
      <div className="row mt-2">
        <div className="col-7 d-flex">
          <img
            src={
              "http://localhost:6969/api/product/image/" +
              product.productImage.image
            }
            alt=""
            className="cartProductImg"
          />
          <div className="flex-column ms-2">
            <h6 className="m-0">{product.productName}</h6>
            <p className="m-0"></p>
            <p className="m-0"></p>
          </div>
        </div>
        <div className="col-2 d-flex align-items-center">
          <p className="m-0">{product.unit}</p>
        </div>
        <div className="col-2 d-flex align-items-center">
          
          {product.productVariant.offer === null ? (
            <p className="m-0">{product.unit * product.productVariant.price}</p>
          ) : (
            <p className="m-0">{product.unit * calculatePrice()}</p>
          )}
        </div>
        <div className="col-1 d-flex justify-content-end align-items-center">
          <FontAwesomeIcon
            style={{ cursor: "pointer" }}
            onClick={deleteProductFromCart}
            icon={faTrash}
          />
        </div>
      </div>
      <hr />
    </div>
  );
}
