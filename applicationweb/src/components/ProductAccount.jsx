import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";

export default function ProductAccount(props) {
  function click() {
    props.deleteProduct(props.id);
  }
  //console.log(props.images[0]);
  function findCustomCategory(id) {
    let name;
    if (props.customCategoryList) {
      for (let i = 0; i < props.customCategoryList.length; i++) {
        if (id === props.customCategoryList[i].id) {
          name = props.customCategoryList[i].name;
        }
      }
    }
    return name;
  }
  return (
    <div className="col-sm-4 col-lg-3 col-xl-2 my-3">
      <div className="card productCard">
        <div className="card-header bg-transparent border-0">
          <h5>{props.name}</h5>
          <p className="m-0">€{props.productVariants[0].price}</p>
        </div>
        <img
          style={{ height: "20vh", objectFit: "contain" }}
          src={
            "http://localhost:6969/api/product/image/" + props.images[0].image
          }
          alt=""
          className="card-img-top productAccountCardImg"
        />
        <hr className="m-1" />
        <div className="card-footer bg-transparent border-0">
          <div className="row">
            <div className="col-10">
              <p className="m-0">Quantité: {props.productVariants[0].unit}</p>
              <p className="m-0">
                Catégorie: {findCustomCategory(props.customCategory)}
              </p>
            </div>
            <div className="col-2 d-flex align-items-center">
              <FontAwesomeIcon
                style={{ cursor: "pointer" }}
                onClick={click}
                icon={faTrash}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
