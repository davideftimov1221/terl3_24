import React from "react";
import axios from "../api/axios";

export default function InProgressOrder(props) {
  function findState() {
    if (props.orderType === "DELIVERY") {
      if (!props.orderState.received && props.orderState.delivered) {
        return "Delivered";
      } else if (props.orderState.received) {
        return "Received";
      } else if (props.orderState.rejected) {
        return "Rejected";
      } else {
        return "Traitement";
      }
    } else {
      if (!props.orderState.received && props.orderState.ready) {
        return "Ready for pickup";
      } else if (props.orderState.received) {
        return "Received";
      } else if (props.orderState.rejected) {
        return "Rejected";
      } else {
        return "Traitement";
      }
    }
  }

  function checkRecieved() {
    if (props.orderType === "DELIVERY") {
      if (!props.orderState.received && props.orderState.delivered) {
        return true;
      } else {
        return false;
      }
    } else {
      if (!props.orderState.received && props.orderState.pickedUp) {
        return true;
      } else {
        return false;
      }
    }
  }

  function orderReceived() {
    const putOrderReceived = async () => {
      const response = await axios.put(
        `http://localhost:6969/api/order/current-user/${props.id}/received`
      );
      //console.log(response.data);
    };
    putOrderReceived();
    props.deleteOrder(props.id);
  }

  return (
    <div className="col-12 col-md-6 col-lg-4 col-xl-3 my-3 p-0 d-flex justify-content-center justify-content-lg-start">
      <div className="card" style={{ width: "20rem" }}>
        <div className="m-1">
          <div className="row">
            <div className="col-6">{props.date.split("T")[0]}</div>
            <div className="col-6 text-end">{findState()}</div>
          </div>
          <hr className="m-2" />

          <div className="row">
            <div className="col-12 d-flex">
              <img
                style={{ height: "8vh", width: "6vw", objectFit: "cover" }}
                src={"http://localhost:6969/api/product/image/" + props.image}
                alt=""
              />
              <div className="flex-column ms-2">
                <h6 className="m-0">{props.storeName}</h6>
                <p className="m-0">{props.storeAddress}</p>
              </div>
            </div>
          </div>

          <hr className="m-2" />

          <div className="row">
            <div className="col-6">
              <p className="m-0">Total:</p>
            </div>
            <div className="col-6 text-end">
              <p className="m-0">{props.bill.total}</p>
            </div>
          </div>
          <div
            className="d-flex justify-content-center"
            style={{ height: "5vh" }}
          >
            {checkRecieved() && (
              <button
                style={{ paddingInline: "2vw" }}
                className="btn btn-sm btn-dark"
                onClick={orderReceived}
              >
                Reçu
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
