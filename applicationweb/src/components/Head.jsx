import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";
import "../styles/head2.css";
import AuthContext from "../context/AuthProvider";
import axios from "../api/axios";
import { useNavigate } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faCartShopping } from "@fortawesome/free-solid-svg-icons";
import ProductContext from "../context/ProductProvider";

export default function Head() {
  const { searchGlobalData, setSearchGlobalData } = useContext(ProductContext);
  const { auth } = useContext(AuthContext);
  const navigate = useNavigate();
  const [searchData, setSearchData] = useState({
    searchText: "",
    radius: 10,
    category: "",
  });

  function handleChangeSearch(event) {
    const { name, value } = event.target;
    setSearchData((prevAddressData) => ({
      ...prevAddressData,
      [name]: value,
    }));
  }
  //console.log(searchData);

  function handleSubmitSearch(event) {
    event.preventDefault();
    setSearchGlobalData(searchData);
    navigate("/search");
  }

  return (
    <header>
      <nav id="mainNavbar" className="navbar navbar-expand-lg navbar-dark">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            <h4 className="brand mb-0">
              Ma <span className="head-title-color">v</span>ille mes{" "}
              <span className="head-title-color">a</span>dresses
            </h4>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                {auth.userType === "simpleUser" && (
                  <form onSubmit={handleSubmitSearch} className="d-lg-flex">
                    <input
                      name="searchText"
                      className="form-control me-2 my-2 my-lg-0"
                      type="search"
                      placeholder="Search"
                      value={searchData.searchText}
                      onChange={handleChangeSearch}
                    />
                    <select
                      className="form-select me-2 my-2 my-lg-0"
                      name="category"
                      value={searchData.category}
                      onChange={handleChangeSearch}
                      id="searchCategory"
                    >
                      <option value="" defaultValue disabled hidden>
                        Categories
                      </option>
                      <option value="Electronics">Electronics</option>
                      <option value="Computers">Computers</option>
                      <option value="WomensFashion">WomensFashion</option>
                      <option value="MensFashion">MensFashion</option>
                      <option value="CameraPhoto">CameraPhoto</option>
                    </select>

                    <div className="d-flex flex-column justify-content-center ms-1 ms-lg-0 me-2 my-2 my-lg-0">
                      <label className="headLabel" htmlFor="customCategory">
                        Rayon: {searchData.radius}
                      </label>
                      <input
                        type="range"
                        min="1"
                        max="100"
                        value={searchData.radius}
                        name="radius"
                        className="slider"
                        id="myRange"
                        onChange={handleChangeSearch}
                      />
                    </div>

                    <button
                      className="btn btn-outline-light border-0 btnSearch my-2 my-lg-0"
                      type="submit"
                      style={{ color: "white" }}
                    >
                      {"Search"}
                    </button>
                  </form>
                )}
              </li>
            </ul>
            <ul className="navbar-nav">
              <li className="nav-item">
                {!auth.token && (
                  <Link
                    className="nav-link"
                    to="/register"
                    style={{ color: "white" }}
                  >
                    Register
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {auth.userType === "simpleUser" && (
                  <Link
                    className="nav-link ms-1 ms-lg-0"
                    to="/cart"
                    style={{ color: "white" }}
                  >
                    <FontAwesomeIcon icon={faCartShopping} inverse />
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {auth.token ? (
                  <Link
                    className="nav-link ms-1 ms-lg-0"
                    to="/account/dashboard"
                    style={{ color: "white" }}
                  >
                    <FontAwesomeIcon icon={faUser} inverse />
                  </Link>
                ) : (
                  <Link
                    className="nav-link"
                    to="/login"
                    style={{ color: "white" }}
                  >
                    Login
                  </Link>
                )}
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
}
