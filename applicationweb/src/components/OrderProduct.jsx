export default function OrderProduct({ name, price, images, quantity }) {
  return (
    <div className="col-sm-4 col-lg-3 col-xl-2">
      <div className="card productCard">
        <div className="card-header bg-transparent border-0">
          <h5>{name}</h5>
          <p className="m-0">€{price}</p>
        </div>
        <img
          style={{ height: "20vh", objectFit: "cover" }}
          src={"http://localhost:6969/api/product/image/" + images}
          alt=""
          className="card-img-top productAccountCardImg"
        />
        <hr className="m-1" />
        <div className="card-footer bg-transparent border-0">
          <div className="row">
            <div className="col-10">
              <p className="m-0">Quantité: {quantity}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
