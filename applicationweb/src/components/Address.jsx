import React from "react"

export default function Address(props) {
    function click (){
        props.deleteAddress(props.id)
    }
    return (
        <div className="col-3">
            <p>{props.streetNumber}, {props.streetName}</p>
            <p>{props.postalCode} {props.locality}, {props.countryName}</p>
            <button className="btn btn-outline-dark mb-3" onClick={click}>Delete</button>
        </div>
    )
}