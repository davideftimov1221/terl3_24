import React, { useState } from "react";

export default function ProductCheckbox(props) {
  function arrayRemove(arr, value) {
    return arr.filter(function (ele) {
      return ele != value;
    });
  }

  function handleChange2(event) {
    const { name, value, type, checked } = event.target;
    props.setCheckList((prevCheckList) => [...prevCheckList, Number(value)]);
  }

  function handleChange3(event) {
    //console.log("Je suis ici")
    const { name, value, type, checked } = event.target;
    props.setCheckList((prevCheckList) =>
      arrayRemove(prevCheckList, Number(value))
    );
  }

  return (
    <div className="col-3">
      {props.checkList.includes(props.productVariants[0].id) ? (
        <input
          type="checkbox"
          id={props.id}
          name="productVariantsId"
          value={props.productVariants[0].id}
          checked={props.checkList.includes(props.productVariants[0].id)}
          onChange={handleChange3}
          className=""
        />
      ) : (
        <input
          type="checkbox"
          id={props.id}
          name="productVariantsId"
          value={props.productVariants[0].id}
          checked={props.checkList.includes(props.productVariants[0].id)}
          onChange={handleChange2}
          className=""
        />
      )}

      <label htmlFor={props.id}>{props.name}</label>
      <br></br>
    </div>
  );
}
