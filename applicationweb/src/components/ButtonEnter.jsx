import { Link } from "react-router-dom";
import "../styles/ButtonEnter.css";

/*-------------
Bouton Entrer ou l'on peut changer le titre du bouton avec la prop 'title'
----------------*/
function ButtonEnter({ title, link, onClick }) {
  return (
    <Link
      onClick={onClick}
      to={link}
      className="button-enter-link btn btn-dark"
    >
      {title}
    </Link>
  );
}

export default ButtonEnter;
