import { faCartPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import photo from "../assets/productImg.jpg";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import "../styles/home2.css";

export default function NoUserHome() {
  const navigate = useNavigate();
  function navigateLogin() {
    navigate("../login");
  }

  function navigateRegister() {
    navigate("../register");
  }
  return (
    <section className="container hehe" id="defaultHome">
      <div className="d-none d-xxl-block firstContent">
               <h1 className="text-end">Explorez dans votre</h1>
              <button className="btn btn-outline-light" onClick={navigateLogin}>
                Login
              </button>
      </div>

      <div className="row">
        <div className="col-0 col-xxl-6" id="first">
          <div className="row">
            <div className="col-2"></div>
            <div className="col-10">

            </div>
          </div>
        </div>
        <div className="col-12 col-xxl-6">
          <div className="row flex-column">
            <div className="d-none d-xxl-block col-12" id="third">

            </div>
            <div className="d-none d-xxl-block col-12" id="fourth">
              <h1 className="d-none d-xxl-block text-start">environnement proche</h1>
            </div>

            <div className="text-center flex-column d-flex d-xxl-none col-12 justify-content-center align-items-center" id="third2">
                  <h1 className="">Explorez dans votre environnement proche</h1>
                  <button className="btn btn-outline-light" onClick={navigateLogin}>
                    Login
                  </button>
            </div>

            <div className="text-center mt-3 flex-column d-flex d-xxl-none col-12 justify-content-center align-items-center" id="fourth2">
                  <h1 className="">Commencez à vendre maintenant</h1>
                  <button className="btn btn-outline-light" onClick={navigateLogin}>
                    Login
                  </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
