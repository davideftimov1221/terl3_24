import AddCustomCategory from "./AddCustomCategory";

export default function AddCustomCategories({
  auth,
  customCategories,
  setCustomCategory,
}) {
  function displayCustomCategories() {
    const elements = [];
    for (let i = 0; i < customCategories.length; i++) {
      const customCategory = customCategories[i];
      elements.push(
        <AddCustomCategory
          auth={auth}
          customcategory={customCategory}
          setCategory={setCustomCategory}
          key={customCategory.id}
        />
      );
    }
    return elements;
  }
  return <div className="w-50">{displayCustomCategories()}</div>;
}
