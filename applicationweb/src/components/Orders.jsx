import { useEffect, useState } from "react";
import OrderProduct from "./OrderProduct";

export default function Orders({ order, deleteOrder }) {
  const [url, setUrl] = useState();
  const [orderState, setOrderState] = useState();
  const [delivered, setDelivered] = useState({
    comment: "",
    date: "",
  });
  const orderReceiver =
    order.receiverFirstName +
    " " +
    order.receiverLastName +
    " born in " +
    order.receiverBirthDay;
  const deliveryAddress = order.address;
  const totalAmount = order.bill.total;
  const paidAmount = order.bill.alreadyPaid;
  const idOrder = order.id;
  const orderstate = order.orderState;
  const orderType = order.orderType;

  function formatDate(date) {
    let k = 0;
    let newDate = "";
    while (k < date.length) {
      if (date[k] !== "T") {
        newDate += date[k];
      } else {
        newDate += " ";
      }
      k++;
    }
    // setDelivered((prevData) => ({ ...prevData, date: newDate }));
    return newDate;
  }
  async function handleSubmit(event) {
    var requestOptions = {
      method: "PUT",
      redirect: "follow",
    };
    if (
      event.target.name !== "Picked Up" &&
      event.target.name !== "Reject" &&
      event.target.name !== "Delivered"
    ) {
      await fetch(url, requestOptions)
        .then((response) => response.text())
        .then((result) => {
          /*console.log(result)*/
        })
        .catch((error) => console.log("error", error));
      deleteOrder(idOrder);
    } else if (
      event.target.name === "Picked Up" ||
      event.target.name === "Delivered"
    ) {
      //console.log(formatDate(delivered.date));
      const urL =
        url +
        "?comment=" +
        delivered.comment +
        "&date=" +
        formatDate(delivered.date);
      if (delivered.date !== "") {
        await fetch(urL, requestOptions)
          .then((response) => response.text())
          .then((result) => {
            /*console.log(result)*/
          })
          .catch((error) => console.log("error", error));
        deleteOrder(idOrder);
      }
    } else if (event.target.name === "Reject") {
      await fetch(url, requestOptions)
        .then((response) => response.text())
        .then((result) => {
          /*console.log(result)*/
        })
        .catch((error) => console.log("error", error));
      deleteOrder(idOrder);
    }
  }

  useEffect(() => {
    const URL = `http://localhost:6969/api/order/current-store/${idOrder}/`;
    if (orderstate.newOrder) {
      setUrl(URL + "accept");
      setOrderState("Accept");
    } else if (orderstate.accepted && !orderstate.ready) {
      setUrl(URL + "ready");
      setOrderState("Ready");
    } else if (
      orderType === "DELIVERY" &&
      orderstate.ready &&
      !orderstate.delivered
    ) {
      setUrl(URL + "delivered");
      setOrderState("Delivered");
    } else if (
      orderType === "SELFPICKUP" &&
      orderstate.ready &&
      !orderstate.delivered
    ) {
      setUrl(URL + "pickedUp");
      setOrderState("Picked Up");
    } else if (orderstate.delivered && !orderstate.received) {
      setOrderState("Received");
    } else if (orderstate.received) {
    }
  }, []);

  return (
    <div className="d-flex flex-column my-4">
      <div className="d-flex">
        <h4 className="m-0 mx-2">Order Receiver : {orderReceiver}</h4>
        {!orderstate.rejected && !orderstate.delivered && !orderstate.pickedUp && (
          <div>
            <button
              onClick={handleSubmit}
              type="button"
              name={orderState}
              id=""
              className="btn btn-dark btn-sm btn-block mx-2"
            >
              {orderState}
            </button>
            <button
              onClick={handleSubmit}
              type="button"
              name="Reject"
              id=""
              className="btn btn-outline-dark btn-sm btn-block mx-2"
            >
              Reject
            </button>
          </div>
        )}
      </div>
      <h5 className="m-0 mx-2">OrderType : {orderType}</h5>
      {deliveryAddress !== null && (
        <h6 className="m-0 mx-2">
          Delivery Address : {deliveryAddress.fullAddress}
        </h6>
      )}

      <div className="d-flex my-4">
        {order.orderProductVariants.map((product) => (
          <OrderProduct
            name={product.productName}
            price={product.productVariant.price}
            images={product.productImage.image}
            quantity={product.quantity}
            key={product.productVariant.id}
          />
        ))}
        <div className="d-flex flex-column justify-content-center mx-4">
          <h6 className="my-2">Total amount : {totalAmount}</h6>
          <h6 className="my-2">Paid amount : {paidAmount}</h6>
        </div>
        {orderstate.ready && !orderstate.delivered && !orderstate.pickedUp && (
          <div className="d-flex flex-column align-items-end justify-content-center border">
            <label htmlFor="" className="m-3">
              Comments
              <input
                onChange={(event) =>
                  setDelivered((prevData) => ({
                    ...prevData,
                    comment: event.target.value,
                  }))
                }
                value={delivered.comment}
                type="text"
                name=""
                id=""
                className="mx-3"
              />
            </label>
            <label className="m-3">
              Date of Order
              <input
                onChange={(event) =>
                  setDelivered((prevData) => ({
                    ...prevData,
                    date: event.target.value,
                  }))
                }
                value={delivered.date}
                type="datetime-local"
                name=""
                id=""
                className="mx-3"
              />
            </label>
          </div>
        )}
      </div>
    </div>
  );
}
