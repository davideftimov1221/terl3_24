import React, { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../context/AuthProvider";

export default function AccountNav() {
  const { auth, sethAuth } = useContext(AuthContext);

  function logout() {
    sethAuth({});
  }

  return (
    <>
      <section id="header" className="jumbotron text-start">
        <h1 className="display-3 m-0">Welcome</h1>
      </section>
      <nav className="navbar navbar-expand-lg navbar-light border-bottom border-2 border-light mt-0">
        <div className="container-fluid">
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link" to="dashboard">
                  Dashboard
                </Link>
              </li>
              <li className="nav-item">
                {auth.userType === "provider" ? (
                  <Link className="nav-link" to="commandes/new">
                    Commandes
                  </Link>
                ) : (
                  <Link className="nav-link" to="commandes/inprogress">
                    Commandes
                  </Link>
                )}
              </li>
              <li className="nav-item">
              {auth.userType === "simpleUser" && (
                <Link className="nav-link" to="adresses">
                  Adresses
                </Link>
              )}
              </li>
              <li className="nav-item">
                {auth.userType === "provider" && (
                  <Link className="nav-link" to="create/customcategory">
                    Créer
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {auth.userType === "provider" && (
                  <Link className="nav-link" to="myproducts">
                    Mes produits
                  </Link>
                )}
              </li>
              <li className="nav-item">
                {auth.userType === "provider" && (
                  <Link className="nav-link" to="offers">
                    Offres
                  </Link>
                )}
              </li>
            </ul>
            <ul className="navbar-nav mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link" to="/" onClick={logout}>
                  Logout
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}
