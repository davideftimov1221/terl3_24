import React from "react"

export default function CustomCatOption(props) {
    return (
        <option value={props.id}>{props.name}</option>
    )
}