import React from "react";
import { createRoot } from "react-dom/client";
import "./styles/index.css";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Account from "./pages/account/Account";
import AccountAdresses from "./pages/account/dashboard/AccountAdresses";
import ChooseService from "./pages/provider/ChooseService";
import CreateStore from "./pages/provider/CreateStore";
import ActivityField from "./pages/category/ActivityField";
import SubCategory from "./pages/category/SubCategory";
import Category from "./pages/category/Category";
import { AuthProvider } from "./context/AuthProvider";
import Dashboard from "./pages/account/dashboard/Dashboard";
import Create from "./pages/provider/account/dashboard/create/Create";
import CustomCategory from "./pages/provider/account/dashboard/create/CustomCategory";
import ProductClient from "./pages/simpleUser/ProductClient";
import Cart from "./pages/simpleUser/Cart";
import Products from "./pages/provider/account/dashboard/Products";
import CreateUser from "./pages/simpleUser/CreateUser";
import Order from "./pages/simpleUser/Order";
import Policy from "./pages/provider/Policy";
import Commandes from "./pages/account/dashboard/Commandes";
import New from "./pages/provider/account/dashboard/commandes/New";
import Ready from "./pages/provider/account/dashboard/commandes/Ready";
import Confirm from "./pages/provider/account/dashboard/commandes/Confirm";
import Accept from "./pages/provider/account/dashboard/commandes/Accept";
import Offers from "./pages/provider/account/dashboard/Offers";

import InProgress from "./pages/simpleUser/InProgress";
import Finalized from "./pages/simpleUser/Finalized";
import History from "./pages/provider/account/dashboard/commandes/History";
import { ProductProvider } from "./context/ProductProvider";

import Search from "./pages/simpleUser/Search";

const root = createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <AuthProvider>
      <ProductProvider>
        <Router>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route path="login" element={<Login />} />
            <Route path="register" element={<Register />} />

            <Route path="chooseservice" element={<ChooseService />} />
            <Route path="createstore" element={<CreateStore />} />
            <Route path="createuser" element={<CreateUser />} />
            <Route path="policy" element={<Policy />} />

            <Route path="account" element={<Account />}>
              <Route path="dashboard" element={<Dashboard />} />
              <Route path="adresses" element={<AccountAdresses />} />
              <Route path="commandes" element={<Commandes />}>
                <Route path="new" element={<New />} />
                <Route path="accept" element={<Accept />} />
                <Route path="ready" element={<Ready />} />
                <Route path="confirmation" element={<Confirm />} />
                <Route path="history" element={<History />} />
                <Route path="inprogress" element={<InProgress />} />
                <Route path="finalized" element={<Finalized />} />
              </Route>
              <Route path="create" element={<Create />}>
                <Route path="customcategory" element={<CustomCategory />} />
              </Route>
              <Route path="myproducts" element={<Products />} />
              <Route path="offers" element={<Offers />} />
            </Route>
            <Route path="activityfield" element={<ActivityField />}>
              <Route path="category" element={<Category />}>
                <Route path="subcategory/:id" element={<SubCategory />} />
              </Route>
            </Route>

            <Route path="product" element={<ProductClient />} />
            <Route path="cart" element={<Cart />} />
            <Route path="order/:storeId" element={<Order />} />
            <Route path="search" element={<Search />} />
          </Routes>
        </Router>
      </ProductProvider>
    </AuthProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals//console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
