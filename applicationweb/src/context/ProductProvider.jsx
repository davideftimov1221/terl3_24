import { createContext, useState } from "react";

const ProductContext = createContext({});

export const ProductProvider = ({ children }) => {
  const [product, setProduct] = useState({});
  const [searchGlobalData, setSearchGlobalData] = useState({});

  return (
    <ProductContext.Provider value={{ product, setProduct, searchGlobalData, setSearchGlobalData}}>
      {children}
    </ProductContext.Provider>
  );
};

export default ProductContext;
