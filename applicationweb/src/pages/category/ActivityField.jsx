import { useEffect, useState } from "react";
import "../../styles/ActivityField.css";
import { Outlet } from "react-router-dom";
import AuthContext from "../../context/AuthProvider";
import { useContext } from "react";

export default function ActivityField() {
  const [categories, setCategories] = useState();
  const { auth } = useContext(AuthContext);

  useEffect(() => {
    const reponse = async () =>
      await fetch("http://localhost:6969/api/category", {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((resp) => resp.json())
        .then((data) => {
          //console.log(data.result);
          setCategories(() => data.result);
        });

    reponse();
  }, []);
  return (
    <div>
      <Outlet context={[categories, auth]} />
    </div>
  );
}
