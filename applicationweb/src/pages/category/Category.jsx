import { useEffect, useState } from "react";
import {
  Navigate,
  Outlet,
  useNavigate,
  useOutletContext,
} from "react-router-dom";
import ButtonEnter from "../../components/ButtonEnter";
import RowsCategories from "../../components/category/RowsCategories";
import "../../styles/Category.css";

export default function Category() {
  const [categories, auth] = useOutletContext();

  const [categoriesChoose, setCategoriesChoose] = useState([]);

  const [checkSubCategories, setCheckSubCategories] = useState(true);

  const navigate = useNavigate();

  function updateStateCategoriesChoose(categories) {
    setCategoriesChoose(categories);
  }

  async function handleClick() {
    if (auth.userType === "provider") {
      const categorieschoose = categoriesChoose.slice();
      let string = categorieschoose[0].subCategoriesChoose[0];

      for (const categories of categorieschoose) {
        for (const subcategory of categories.subCategoriesChoose) {
          if (subcategory !== string) {
            string = string + "," + subcategory;
          }
        }
      }

      //console.log(string);

      let formdata = new FormData();
      formdata.append("id", auth.id);
      formdata.append("categories", string);

      let requestOptions = {
        method: "POST",
        body: formdata,
        redirect: "follow",
      };

      await fetch("http://localhost:6969/api/store/category", requestOptions)
        .then((response) => response.text())
        .then((result) => {
          /*console.log(result)*/
        })
        .catch((error) => console.log("error", error));
    } else if (auth.userType === "simpleUser") {
      let userCategories = { id: auth.id, interest: [] };
      let cat = [];
      categoriesChoose
        .map((category) => category.subCategoriesChoose)
        .map((subcategories) => cat.push(subcategories));
      //console.log(cat);

      for (const subcategories of cat) {
        for (const subcategory of subcategories) {
          userCategories.interest.push(subcategory);
        }
      }

      let myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");

      let raw = JSON.stringify(userCategories);

      let requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };

      await fetch(
        "http://localhost:6969/api/user/interestCenter",
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          /*console.log(result)*/
        })
        .catch((error) => console.log("error", error));
      //console.log(userCategories);
    }
    navigate("../../");
  }

  //useEffect(() => console.log(categoriesChoose), [categoriesChoose]);

  return (
    <>
      {!auth.token ? (
        <Navigate to="../../login" />
      ) : (
        <div>
          {checkSubCategories && (
            <div className="container d-flex flex-column justify-content-center align-items-center">
              <div className="activityfield-title">
                <h1 className="activityfield-title">
                  What are your fields of activity ?
                </h1>
              </div>
              <div className="category-rows">
                <RowsCategories
                  categories={categories}
                  nbCategoriesPerRow={3}
                  categoriesChoose={categoriesChoose}
                  setCategoriesChoose={updateStateCategoriesChoose}
                  setCheckSubCategories={setCheckSubCategories}
                />
              </div>
              <div className="mt-5">
                {categoriesChoose.length !== 0 && (
                  <ButtonEnter title="Save" link="" onClick={handleClick} />
                )}
              </div>
            </div>
          )}
          <Outlet
            context={[
              categories,
              categoriesChoose,
              updateStateCategoriesChoose,
              setCheckSubCategories,
              auth,
            ]}
          />
        </div>
      )}
    </>
  );
}
