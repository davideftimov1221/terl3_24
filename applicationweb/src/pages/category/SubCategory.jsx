import { useEffect, useState } from "react";
import { Navigate, useOutletContext, useParams } from "react-router-dom";
import ButtonEnter from "../../components/ButtonEnter";
import RowsSubCategories from "../../components/category/subcategory/RowsSubCategories";
import "../../styles/SubCategory.css";

export default function SubCategory() {
  const [
    categories,
    categoriesChoose,
    setCategoriesChoose,
    setCheckSubCategories,
    auth,
  ] = useOutletContext();
  const params = useParams();
  const categoryId = Number(params.id);
  const indexCategories = categoriesChoose.findIndex(
    (element) => element.id === categoryId
  );
  const [subCategories, setSubCategories] = useState(
    categoriesChoose[indexCategories].subCategoriesChoose.slice()
  );
  const subcategories =
    categories[
      categories.findIndex((result) => result.id === Number(params.id))
    ].subCategorys;

  function funSetCategoriesChoose(subcategories) {
    setSubCategories(subcategories);
  }
  useEffect(() => {
    const categorieschoose = categoriesChoose.slice();
    categorieschoose[indexCategories].subCategoriesChoose =
      subCategories.slice();
    setCategoriesChoose(categorieschoose);
  }, [subCategories]);
  return (
    <>
      {!auth.token ? (
        <Navigate to="../../login" />
      ) : (
        <div className="container d-flex flex-column justify-content-center align-items-center">
          <div className="activityfield-title">
            <h1 className="activityfield-title">Choose sub-fields</h1>
          </div>
          <div className="subcategory-rows">
            {
              <RowsSubCategories
                subcategories={subcategories}
                nbSubCategoriesPerRow={2}
                subCategories={subCategories}
                setCategoriesChoose={funSetCategoriesChoose}
              />
            }
          </div>
          <div className="mt-5">
            {subCategories.length !== 0 && (
              <ButtonEnter
                onClick={() => setCheckSubCategories(true)}
                title="Next"
                link="../../category"
              />
            )}
          </div>
        </div>
      )}
    </>
  );
}
