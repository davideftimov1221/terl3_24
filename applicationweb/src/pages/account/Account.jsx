import "../../styles/Account.css";
import { useContext, useEffect, useState } from "react";
import { Navigate, Outlet } from "react-router-dom";
import AuthContext from "../../context/AuthProvider";

import Head from "../../components/Head";
import Footer from "../../components/Footer";
import AccountNav from "../../components/AccountNav";

export default function Account() {
  const { auth } = useContext(AuthContext);
  const [clickCategory, setClickCategory] = useState({
    clickAddCategory: false,
    clickUpdateCategory: false,
    clickDeleteCategory: false,
  });
  const [customCategory, setCustomCategory] = useState({
    id: 0,
    userId: "",
    provider: auth.id,
    valid: "",
  });

  function funSetClickCategory(click) {
    let clickcategory;
    if (click === "add") {
      clickcategory = clickCategory.clickAddCategory;
      clickcategory === false
        ? (clickcategory = true)
        : (clickcategory = false);
      setClickCategory((prevData) => ({
        ...prevData,
        clickAddCategory: clickcategory,
      }));
    } else if (click === "update") {
      clickcategory = clickCategory.clickUpdateCategory;
      clickcategory === false
        ? (clickcategory = true)
        : (clickcategory = false);
      setClickCategory((prevData) => ({
        ...prevData,
        clickUpdateCategory: clickcategory,
      }));
    } else if (click === "delete") {
      clickcategory = clickCategory.clickDeleteCategory;
      clickcategory === false
        ? (clickcategory = true)
        : (clickcategory = false);
      setClickCategory((prevData) => ({
        ...prevData,
        clickDeleteCategory: clickcategory,
      }));
    }
  }

  function handleClick() {
    funSetClickCategory(customCategory.valid);
    setCustomCategory((prevData) => ({
      ...prevData,
      valid: "",
    }));
    //console.log();
  }

  useEffect(() => {
    if (customCategory.valid === "delete") {
      funSetClickCategory(customCategory.valid);
      setCustomCategory((prevData) => ({ ...prevData, valid: "" }));
    }
  }, [customCategory.valid]);

  return (
    <>
      {!auth.token ? (
        <Navigate to="../login" />
      ) : (
        <div>
          <div className="content">
            <Head />
            <AccountNav />
            <Outlet
              context={[
                auth,
                clickCategory,
                funSetClickCategory,
                customCategory,
                setCustomCategory,
              ]}
            />
          </div>
          <Footer />
        </div>
      )}
      {(customCategory.valid === "add" ||
        customCategory.valid === "update") && (
        <div className=" row  align-items-center justify-content-center create-background">
          <div className="d-flex flex-column align-items-center justify-content-spacearound create">
            {!clickCategory.clickUpdateCategory ? (
              <h4>New</h4>
            ) : (
              <h4>Update</h4>
            )}
            <input
              type="text"
              className="form-control"
              name=""
              id=""
              value={customCategory.name}
              onChange={(event) =>
                setCustomCategory((prevData) => ({
                  ...prevData,
                  name: event.target.value,
                }))
              }
              placeholder="Name"
            />
            <button
              onClick={handleClick}
              type="button"
              name=""
              id=""
              className="btn btn-primary btn-lg btn-block"
            >
              Save
            </button>
          </div>
        </div>
      )}
    </>
  );
}
