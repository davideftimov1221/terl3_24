import { useOutletContext } from "react-router-dom";
import React, { useState, useEffect } from "react";
import axios from "../../../api/axios";
import Address from "../../../components/Address";

export default function AccountAdresses() {
  const [auth] = useOutletContext();
  const [newAddress, setNewAddress] = useState(false);
  const [newAddressAlert, setNewAddressAlert] = useState(false);
  //console.log(auth.id);
  function setAddressVarT() {
    setNewAddress(true);
  }

  function setAddressVarF() {
    setNewAddress(false);
  }
  /*ADD ADDRESS */
  const [addressData, setAddressData] = useState({
    streetNumber: "",
    streetName: "",
    postalCode: "",
    locality: "",
    countryName: "",
    userId: auth.id,
  });

  function handleChange(event) {
    setAddressData((prevAddressData) => ({
      ...prevAddressData,
      [event.target.name]: event.target.value,
    }));
  }
  //console.log(addressData);

  function handleSubmit(event) {
    event.preventDefault();
    //console.log(addressData);
    fetch("http://localhost:6969/api/address/create", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(addressData),
    }).then(() => {
      //console.log("New address added");
      setNewAddressAlert(!newAddressAlert);
      setNewAddress(false);
    });
  }
  /*ADD ADDRESS */

  const deleteAddress = (id) => {
    axios.delete("http://localhost:6969/api/address/delete/" + id).then(() => {
      alert("Address deleted!");
      setNewAddressAlert(!newAddressAlert);
    });
  };

  /*RENDER ADDRESSES */
  const [addressList, setAddressList] = React.useState();
  useEffect(() => {
    const getAdresses = async () => {
      const response = await axios.get(
        "http://localhost:6969/api/address/" + auth.id
      );
      setAddressList(response.data.result);
    };
    getAdresses();
  }, [newAddressAlert]);

  function rows() {
    let addressDivList;
    //console.log("Rows");
    if (addressList) {
      addressDivList = addressList.map((address) => {
        return (
          <Address
            key={address.id}
            id={address.id}
            streetNumber={address.streetNumber}
            streetName={address.streetName}
            postalCode={address.postalCode}
            locality={address.locality}
            countryName={address.countryName}
            deleteAddress={deleteAddress}
          />
        );
      });
    }
    return addressDivList;
  }

  //console.log(addressList);
  /*RENDER ADDRESSES */

  return (
    <>
      {!newAddress ? (
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-8 d-flex align-items-center">
              <h3 className="m-0">Vos adresses</h3>
            </div>
            <div className="col-4 d-flex align-items-center">
              <button className="btn btn-dark" onClick={setAddressVarT}>
                Ajouter une nouvelle adresse
              </button>
            </div>
          </div>
          <hr />
          <div className="row">{rows()}</div>
        </div>
      ) : (
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-3">
              <button className="btn btn-outline-dark" onClick={setAddressVarF}>
                {" "}
                {"<- Retour aux adresses"}
              </button>
            </div>
            <div className="col-9">
              <h5>Ajouter une nouvelle adresse</h5>
              <form onSubmit={handleSubmit} className="">
                <div className="row">
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Numéro de rue"
                      onChange={handleChange}
                      name="streetNumber"
                      value={addressData.streetNumber}
                      className="form-control mt-2"
                    />
                  </div>
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Nom de rue"
                      onChange={handleChange}
                      name="streetName"
                      value={addressData.streetName}
                      className="form-control mt-2"
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Code postal"
                      onChange={handleChange}
                      name="postalCode"
                      value={addressData.postalCode}
                      className="form-control mt-2"
                    />
                  </div>
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Ville"
                      onChange={handleChange}
                      name="locality"
                      value={addressData.locality}
                      className="form-control mt-2"
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Pays"
                      onChange={handleChange}
                      name="countryName"
                      value={addressData.countryName}
                      className="form-control mt-2"
                    />
                  </div>
                  <div className="col-6"></div>
                </div>

                <button className="btn btn-dark mt-2">Ajouter adresse</button>
              </form>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
