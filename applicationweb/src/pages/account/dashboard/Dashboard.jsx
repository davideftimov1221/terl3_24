import { useOutletContext } from "react-router-dom";
import axios from "axios";
import React, { useContext, useState, useEffect } from "react";

export default function Dashboard() {
  const [auth] = useOutletContext();
  const [userInfo, setUserInfo] = React.useState();
  const [defaultCity, setDefaultCity] = React.useState(true);

  useEffect(() => {
    if(auth.userType === "simpleUser"){
      const getUserInfo = async () => {
        const response = await axios.get(
          "http://localhost:6969/api/user/Information/" + auth.id
        );
        setUserInfo(response.data);
      };

      const getCityInfo = async () => {
        const response2 = await axios.get(
          `http://localhost:6969/api/user/default-city?id=${auth.id}`
        );
        setDefaultCity(response2.data);
      };
      getCityInfo();
      getUserInfo();
    }

  }, []);

  



  
  return (
    <>
    {(auth.userType === "simpleUser") ?
    (<div>
      <div className="card border-0" style={{width : "18rem"}}>
        <div className="card-body">
          <h6 className="card-subtitle mb-2">Nom: {userInfo && userInfo.lastName}</h6>
          <h6 className="card-subtitle mb-2">Prénom: {userInfo && userInfo.firstName}</h6>
          <h6 className="card-subtitle mb-2">Date de naissance: {userInfo && userInfo.birthDay}</h6>
          <h6 className="card-subtitle mb-2">Ville: {defaultCity && defaultCity.displayName}</h6>
        </div>
      </div>
    </div>) : <p className="mt-2 ms-2">Votre email est {auth.email}</p>
  }
  </>
  );
}
