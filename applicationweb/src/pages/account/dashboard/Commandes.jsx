import { useEffect, useState } from "react";
import { Link, Outlet, useOutletContext } from "react-router-dom";

export default function Commandes() {
  const [auth] = useOutletContext();
  const [nav, setNav] = useState();

  useEffect(() => {
    if (auth.userType === "provider") {
      setNav("New");
    } else {
      setNav("InProgress");
    }
  }, []);

  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-light bg-light justify-content-center">
        <ul className="navbar-nav">
          {auth.userType === "provider" && (
            <li className="nav-item">
              <Link
                onClick={() => setNav("New")}
                className={nav === "New" ? "nav-link active" : "nav-link"}
                to="new"
              >
                Nouveau
              </Link>
            </li>
          )}
          {auth.userType === "provider" && (
            <li className="nav-item">
              <Link
                onClick={() => setNav("Accept")}
                className={nav === "Accept" ? "nav-link active" : "nav-link"}
                to="accept"
              >
                Accepter
              </Link>
            </li>
          )}
          {auth.userType === "provider" && (
            <li className="nav-item">
              <Link
                onClick={() => setNav("Ready")}
                className={nav === "Ready" ? "nav-link active" : "nav-link"}
                to="ready"
              >
                Prêt
              </Link>
            </li>
          )}
          {auth.userType === "provider" && (
            <li className="nav-item">
              <Link
                onClick={() => setNav("Confirm")}
                className={nav === "Confirm" ? "nav-link active" : "nav-link"}
                to="confirmation"
              >
                Confirmation en attente
              </Link>
            </li>
          )}
          {auth.userType === "provider" && (
            <li className="nav-item">
              <Link
                onClick={() => setNav("History")}
                className={nav === "History" ? "nav-link active" : "nav-link"}
                to="history"
              >
                Histoire
              </Link>
            </li>
          )}
          <li className="nav-item">
            {auth.userType === "simpleUser" && (
              <Link
                onClick={() => setNav("InProgress")}
                className={
                  nav === "InProgress" ? "nav-link active" : "nav-link"
                }
                to="inprogress"
              >
                En cours
              </Link>
            )}
          </li>
          <li className="nav-item">
            {auth.userType === "simpleUser" && (
              <Link
                onClick={() => setNav("Finalized")}
                className={nav === "Finalized" ? "nav-link active" : "nav-link"}
                to="finalized"
              >
                Finalisé
              </Link>
            )}
          </li>
        </ul>
      </nav>
      <Outlet context={[auth]} />
    </div>
  );
}
