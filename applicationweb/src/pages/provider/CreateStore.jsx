import { useContext, useEffect, useState } from "react";
import PhoneInputWithCountrySelect from "react-phone-number-input";
import "../../styles/CreateStore.css";
import "react-phone-number-input/style.css";
import AuthContext from "../../context/AuthProvider";
import { Navigate, useNavigate } from "react-router-dom";
import FormData from "form-data";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationCrosshairs } from "@fortawesome/free-solid-svg-icons";

const CREATESTORE_URL = "http://localhost:6969/api/store/create";

export default function CreateStore() {
  const { auth } = useContext(AuthContext);

  const [storeData, setStoreData] = useState({
    userId: "",
    description: "",
    storeAddress: {},
    provider: auth.id,
    imageStore: "",
    telephoneNumber: "",
    defaultTelephoneNumber: "",
  });
  const [storeAddress, setStoreAddress] = useState("");
  const [image, setImage] = useState();
  const [telephoneNumber, setTelephoneNumber] = useState();
  const [defaultTelephoneNumber, setDefaultTelephoneNumber] = useState();

  const navigate = useNavigate();

  async function handleChange(event) {
    let storedata = storeData;
    if (event.currentTarget.name === "storeAddress") {
      setStoreAddress(event.target.value);

      if (storeAddress.length > 15) {
        let requestOptions = {
          method: "GET",
          redirect: "follow",
        };
        let geodata;
        await fetch(
          `http://api.positionstack.com/v1/forward?access_key=e722c14b6990095206adfd4d3678242c&query=${event.target.value}`,
          requestOptions
        )
          .then((response) => response.text())
          .then((result) => {
            geodata = JSON.parse(result);
            //console.log(geodata);
          })
          .catch((error) => console.log("error", error));

        geodata = geodata.data;
        geodata = geodata.filter((data) => data.number !== null);

        if (geodata[0]) {
          geodata = geodata[0];
          geodata = {
            streetNumber: geodata.number,
            admin: geodata.region,
            subAdmin: geodata.administrative_area,
            locality: geodata.locality,
            streetName: geodata.street,
            postalCode: geodata.postal_code,
            countryCode: geodata.country_code,
            coutryName: geodata.country,
            latitude: geodata.latitude,
            longitude: geodata.longitude,
            fullAddress: geodata.label,
          };

          storedata.storeAddress = geodata;
          setStoreData(storedata);
          //console.log(storedata.storeAddress);
        }
      }
    } else {
      const { name, value, type, files } = event.target;
      type === "file" && setImage(files[0]);
      storedata = (prevStoreData) => ({
        ...prevStoreData,
        [name]: type === "file" ? files[0].name : value,
      });
      setStoreData(storedata);
    }
  }
  //console.log(storeData);

  async function handleGeoPosition() {
    const success = async (position) => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;
      //console.log(latitude + " " + longitude);

      let requestOptions = {
        method: "GET",
        redirect: "follow",
      };
      let geodata;
      await fetch(
        `http://api.positionstack.com/v1/reverse?access_key=e722c14b6990095206adfd4d3678242c&query=${latitude},${longitude}&limit=3`,
        requestOptions
      )
        .then((response) => response.json())
        .then((result) => {
          geodata = result;
          //console.log(geodata);
        })
        .catch((error) => console.log("error", error));

      geodata = geodata.data;
      geodata = geodata.filter((data) => data.number !== null);

      if (geodata[0]) {
        geodata = geodata[0];
        geodata = {
          streetNumber: geodata.number,
          admin: geodata.region,
          subAdmin: geodata.administrative_area,
          locality: geodata.locality,
          streetName: geodata.street,
          postalCode: geodata.postal_code,
          countryCode: geodata.country_code,
          coutryName: geodata.country,
          latitude: geodata.latitude,
          longitude: geodata.longitude,
          fullAddress: geodata.label,
        };
        const storedata = storeData;
        storedata.storeAddress = geodata;
        setStoreData(storedata);
        setStoreAddress(geodata.fullAddress);
        //console.log(storedata.storeAddress);
      }
    };
    const error = () => {};

    navigator.geolocation.getCurrentPosition(success, error);
  }
  async function handleSubmit(event) {
    event.preventDefault();

    //console.log(telephoneNumber);

    const storedata = storeData;
    storedata.telephoneNumber = telephoneNumber;
    storedata.defaultTelephoneNumber = defaultTelephoneNumber;
    setStoreData(storedata);
    //console.log(storedata);

    const fileStore = new File([JSON.stringify(storeData)], "store.json", {
      type: "application/json",
    });

    let formdata = new FormData();
    formdata.append("store", fileStore);
    formdata.append("image", image);

    let requestOptions = {
      method: "POST",
      body: formdata,
      redirect: "follow",
    };

    let error;
    await fetch(CREATESTORE_URL, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        //console.log(result);
        error = JSON.parse(result);
      });
    if (!error.errorResponse) {
      navigate("../policy");
    } else {
    }
  }
  useEffect(() => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };
    var error;
    const response = async () => {
      await fetch(
        `http://localhost:6969/api/store/Information/${auth.id}`,
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          //console.log(result);
          error = JSON.parse(result);
        });
      if (error.errorResponse !== "StoreNotFound") {
        navigate("/");
      }
    };
    response();
  }, []);
  return (
    <>
      {!auth.token ? (
        <Navigate to="../login" />
      ) : (
        <div className="container createstore">
          <h1 className="createstore-title">Create Store</h1>
          <form
            onSubmit={handleSubmit}
            className="d-flex flex-column justify-content-center align-items-center"
          >
            <div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control border-0 border-bottom createstore-text-enter"
                  name="name"
                  value={storeData.name}
                  placeholder="Store Name"
                  onChange={handleChange}
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control border-0 border-bottom createstore-text-enter"
                  name="description"
                  value={storeData.description}
                  placeholder="Store Description"
                  onChange={handleChange}
                />
              </div>
              <div className="form-group d-flex align-items-center">
                <input
                  type="text"
                  className="form-control border-0 border-bottom createstore-text-enter"
                  list="suggestion"
                  name="storeAddress"
                  value={storeAddress}
                  placeholder="Address"
                  onChange={handleChange}
                />
                <datalist id="suggestion">
                  <option value={storeData.storeAddress.fullAddress} />
                </datalist>
                <FontAwesomeIcon
                  onClick={handleGeoPosition}
                  className=""
                  size="2x"
                  icon={faLocationCrosshairs}
                />
              </div>

              <div className="form-group">
                <label htmlFor="storePhoto">Sélectionner une image</label>
                <input
                  className="form-control border-0 border-bottom createstore-text-enter"
                  type="file"
                  name="imageStore"
                  onChange={handleChange}
                  id="storePhoto"
                />
              </div>

              <div className="form-group">
                <small id="helpId" className="form-text text-muted">
                  Phone Number
                </small>
                <PhoneInputWithCountrySelect
                  international
                  countryCallingCodeEditable={false}
                  value={telephoneNumber}
                  onChange={setTelephoneNumber}
                  limitMaxLength
                  className="form-control border-0 border-bottom createstore-text-enter"
                  defaultCountry="FR"
                />

                <small id="helpId" className="form-text text-muted">
                  *Required Phone Number for Smart City
                </small>
              </div>
              <div className="form-group">
                <PhoneInputWithCountrySelect
                  international
                  countryCallingCodeEditable={false}
                  value={defaultTelephoneNumber}
                  onChange={setDefaultTelephoneNumber}
                  limitMaxLength
                  className="form-control border-0 border-bottom createstore-text-enter"
                  defaultCountry="FR"
                />
              </div>
            </div>
            <div className="mt-5">
              <button className="btn btn-dark createstore-button-next">
                Next
              </button>
            </div>
          </form>
        </div>
      )}
    </>
  );
}
