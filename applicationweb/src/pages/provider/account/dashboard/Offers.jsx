import { useOutletContext } from "react-router-dom";
import React, { useState, useEffect } from "react";
import axios from "../../../../api/axios";
import Offer from "../../../../components/Offer";
import ProductCheckbox from "../../../../components/ProductCheckbox";

export default function Offers() {
  const [auth] = useOutletContext();
  const [newOffer, setNewOffer] = useState(false);
  const [newOfferAlert, setNewOfferAlert] = useState(false);

  const [checkList, setCheckList] = useState([]);
  //console.log(auth.id);
  function setAddressVarT() {
    setNewOffer(true);
  }

  function setAddressVarF() {
    setNewOffer(false);
  }
  /*ADD OFFER */
  const [offerData, setOfferData] = useState({
    discountCode: "",
    type: "",
    newPrice: "",
    percentage: "",
    startDate: "",
    endDate: "",
    providerId: auth.id,
    productVariantsId: [],
  });

  function handleChange(event) {
    const { name, value, type } = event.target;
    setOfferData((prevOfferData) => ({
      ...prevOfferData,
      [name]: type === "datetime-local" ? value.replace("T", " ") : value,
    }));
  }
  //console.log(offerData);

  function handleSubmit(event) {
    event.preventDefault();
    //console.log(offerData);
    fetch("http://localhost:6969/api/offer/create", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(offerData),
    }).then(() => {
      //console.log("New offer added");
      setNewOfferAlert(!newOfferAlert);
      setNewOffer(false);
    });
  }
  /*ADD OFFER */

  /* GET ALL PRODUCTS */

  const [productList, setProductList] = React.useState();
  useEffect(() => {
    const getProducts = async () => {
      const response = await axios.get(
        "http://localhost:6969/api/product/all/provider/" + auth.id
      );
      setProductList(response.data.result);
    };
    getProducts();
  }, []);

  useEffect(() => {
    setOfferData((prevOfferData) => ({
      ...prevOfferData,
      productVariantsId: checkList,
    }));
    //console.log(offerData);
  }, [checkList]);

  function productCheckbox() {
    let productDivList;
    //console.log("Rows");
    if (productList) {
      productDivList = productList.map((product) => {
        return (
          <ProductCheckbox
            key={product.id}
            id={product.id}
            productVariants={product.productVariants}
            name={product.name}
            checkList={checkList}
            setCheckList={setCheckList}
          />
        );
      });
    }
    return productDivList;
  }

  /* GET ALL PRODUCTS */

  /* DELETE OFFER */
  const deleteOffer = (id) => {
    axios.delete("http://localhost:6969/api/offer/delete/" + id).then(() => {
      alert("Offer deleted!");
      setNewOfferAlert(!newOfferAlert);
    });
  };
  /* DELETE OFFER */

  /*RENDER OFFERS */
  const [offersList, setOffersList] = React.useState();
  useEffect(() => {
    const getOffers = async () => {
      const response = await axios.get(
        "http://localhost:6969/api/offer/current-provider-offers?id=" + auth.id
      );
      setOffersList(response.data.result);
    };
    getOffers();
  }, [newOfferAlert]);

  function rows() {
    let offerDivList;
    //console.log("Rows");
    if (offersList) {
      offerDivList = offersList.map((offer) => {
        return (
          <Offer
            key={offer.id}
            id={offer.id}
            name={offer.discountCode}
            products={offer.products}
            deleteOffer={deleteOffer}
          />
        );
      });
    }
    return offerDivList;
  }

  //console.log(offersList);
  /*RENDER OFFERS */

  return (
    <>
      {!newOffer ? (
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-8 d-flex align-items-center">
              <h3 className="m-0">Vos offres</h3>
            </div>
            <div className="col-4 d-flex align-items-center">
              <button className="btn btn-dark" onClick={setAddressVarT}>
                Ajouter une nouvelle offre
              </button>
            </div>
          </div>
          <hr />
          <div className="row">{rows()}</div>
        </div>
      ) : (
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-3">
              <button className="btn btn-outline-dark" onClick={setAddressVarF}>
                {" "}
                {"<- Retour aux adresses"}
              </button>
            </div>
            <div className="col-9">
              <h5>Ajouter une nouvelle offre</h5>
              <form onSubmit={handleSubmit} className="">
                <div className="row">
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Nom de l'offre"
                      onChange={handleChange}
                      name="discountCode"
                      value={offerData.discountCode}
                      className="form-control mt-2"
                    />
                  </div>
                  <div className="col-6"></div>
                </div>

                <div className="row mt-2">
                  <div className="col-6">
                    <label htmlFor="type">Sélectionner le type d'offre</label>
                    <select
                      name="type"
                      value={offerData.type}
                      onChange={handleChange}
                      id="type"
                      className="form-control"
                    >
                      <option value="" defaultValue disabled hidden>
                        Choisir ici
                      </option>

                      <option value={"FIXED"}>Montant fixé</option>

                      <option value={"PERCENTAGE"}>Pourcentage</option>
                    </select>
                  </div>
                  <div className="col-6">
                    {offerData.type === "FIXED" ? (
                      <input
                        type="number"
                        placeholder="Montant"
                        onChange={handleChange}
                        name="newPrice"
                        value={offerData.newPrice}
                        className="form-control mt-4"
                      />
                    ) : (
                      <input
                        type="number"
                        placeholder="Pourcentage"
                        onChange={handleChange}
                        name="percentage"
                        value={offerData.percentage}
                        className="form-control mt-4"
                      />
                    )}
                  </div>
                </div>

                <div className="row mt-2">
                  <div className="col-6">
                    <label htmlFor="startDate">Date de début</label>
                    <input
                      type="datetime-local"
                      placeholder="Date de début"
                      onChange={handleChange}
                      name="startDate"
                      value={offerData.startDate.replace(" ", "T")}
                      className="form-control"
                      id="startDate"
                    />
                  </div>
                  <div className="col-6">
                    <label htmlFor="endDate">Date de fin</label>
                    <input
                      type="datetime-local"
                      placeholder="Date de fin"
                      onChange={handleChange}
                      name="endDate"
                      value={offerData.endDate.replace(" ", "T")}
                      className="form-control"
                      id="endDate"
                    />
                  </div>
                </div>

                <div className="row">
                  <p className="mt-2 mb-1">Choisir des produits</p>
                  {productCheckbox()}
                </div>

                <button className="btn btn-dark mt-2">Ajouter offre</button>
              </form>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
