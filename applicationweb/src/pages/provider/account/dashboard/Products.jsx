import { useOutletContext } from "react-router-dom";
import React, { useState, useEffect } from "react";
import axios from "../../../../api/axios";
import FormData from "form-data";
import CustomCatOption from "../../../../components/CustomCatOption";
import ProductAccount from "../../../../components/ProductAccount";
import { Navigate } from "react-router-dom";

export default function Products() {
  const [auth] = useOutletContext();
  const [newProduct, setNewProduct] = useState(false);
  const [newProductAlert, setNewProductAlert] = useState(false);
  //console.log(auth.id);
  function setProductVarT() {
    setNewProduct(true);
  }

  function setProductVarF() {
    setNewProduct(false);
  }

  /*GET CUSTOM CATEGORIES */
  const [customCategoryList, setCustomCategoryList] = React.useState();
  useEffect(() => {
    const getCustomCategories = async () => {
      const response = await axios.get(
        "http://localhost:6969/api/store/customCategory/all/" + auth.id
      );
      setCustomCategoryList(response.data.result);
    };
    getCustomCategories();
  }, []);

  function customCategoriesMap() {
    let customCategoriesOptionList;
    //console.log("Rows");
    if (customCategoryList) {
      customCategoriesOptionList = customCategoryList.map((category) => {
        return (
          <CustomCatOption
            key={category.id}
            id={category.id}
            name={category.name}
          />
        );
      });
    }
    return customCategoriesOptionList;
  }
  //console.log(customCategoryList);

  /*GET CUSTOM CATEGORIES */

  /*ADD PRODUCT */
  const [variantsData, setVariantsData] = useState({
    price: "",
    unit: "",
  });

  useEffect(() => {
    setProductData((prevAddressData) => ({
      ...prevAddressData,
      productVariants: [variantsData],
    }));
  }, [variantsData]);

  function handleChangeVariants(event) {
    const { name, value } = event.target;
    setVariantsData((prevVariantsData) => ({
      ...prevVariantsData,
      [name]: value,
    }));
  }

  const [productData, setProductData] = useState({
    description: "",
    userId: "",
    customCategory: "",
    images: "",
    productVariants: [variantsData],
  });
  const [images, setImages] = useState();

  function handleChange(event) {
    const { name, value, type, files } = event.target;
    type === "file" && setImages(files[0]);
    setProductData((prevAddressData) => ({
      ...prevAddressData,
      [name]:
        type === "file"
          ? Object.keys(files).map((file) => {
              return files[file].name;
            })
          : value,
    }));
  }
  //console.log(productData);
  //console.log(images);

  async function handleSubmit(event) {
    event.preventDefault();
    //console.log(productData);

    const fileProduct = new File([JSON.stringify(productData)], "store.json", {
      type: "application/json",
    });
    const fileImage2 = new File(["null"], "image2.txt", { type: "text/plain" });

    var formdata = new FormData();
    formdata.append("product", fileProduct);
    formdata.append("productImagesFile", images);
    formdata.append("variantesImagesFile", fileImage2);

    await fetch("http://localhost:6969/api/product/create", {
      method: "POST",
      body: formdata,
      redirect: "follow",
    }).then(() => {
      //console.log("New product added");
      setNewProductAlert(!newProductAlert);
      setNewProduct(false);
    });
  }
  /*ADD PRODUCT */

  const deleteProduct = (id) => {
    axios.delete("http://localhost:6969/api/product/delete/" + id).then(() => {
      alert("Product deleted!");
      setNewProductAlert(!newProductAlert);
    });
  };

  /*RENDER ADDRESSES */
  const [productList, setProductList] = React.useState();
  useEffect(() => {
    const getProducts = async () => {
      const response = await axios.get(
        "http://localhost:6969/api/product/all/provider/" + auth.id
      );
      setProductList(response.data.result);
    };
    getProducts();
  }, [newProductAlert]);

  function rows() {
    let productDivList;
    //console.log("Rows");
    if (productList) {
      productDivList = productList.map((product) => {
        return (
          <ProductAccount
            key={product.id}
            id={product.id}
            productVariants={product.productVariants}
            description={product.description}
            name={product.name}
            images={product.images}
            customCategory={product.customCategory}
            deleteProduct={deleteProduct}
            customCategoryList={customCategoryList}
          />
        );
      });
    }
    return productDivList;
  }

  //console.log(productList);
  /*RENDER ADDRESSES */

  function testCustomCategoryList() {
    let bool = false;
    if (customCategoryList) {
      if (customCategoryList.length === 0) {
        bool = true;
      }
    }
    return bool;
  }

  return (
    <>
      {!newProduct ? (
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-8 d-flex align-items-center">
              <h3 className="m-0">Vos produits</h3>
            </div>
            <div className="col-4 d-flex align-items-center">
              <button className="btn btn-dark" onClick={setProductVarT}>
                Ajouter un nouveau produit
              </button>
            </div>
          </div>
          <hr />
          <div className="row">{rows()}</div>
        </div>
      ) : testCustomCategoryList() ? (
        <Navigate to="../create/customcategory" />
      ) : (
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-3">
              <button className="btn btn-outline-dark" onClick={setProductVarF}>
                {" "}
                {"<- Retour aux produits"}
              </button>
            </div>
            <div className="col-9">
              <h5>Ajouter un nouveau produit</h5>
              <form onSubmit={handleSubmit} className="">
                <div className="row">
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Nom du produit"
                      onChange={handleChange}
                      name="name"
                      value={productData.name}
                      className="form-control mt-2"
                    />
                  </div>
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Description du produit"
                      onChange={handleChange}
                      name="description"
                      value={productData.description}
                      className="form-control mt-2"
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Prix"
                      onChange={handleChangeVariants}
                      name="price"
                      value={variantsData.price}
                      className="form-control mt-2"
                    />
                  </div>
                  <div className="col-6">
                    <input
                      type="text"
                      placeholder="Quantité"
                      onChange={handleChangeVariants}
                      name="unit"
                      value={variantsData.unit}
                      className="form-control mt-2"
                    />
                  </div>
                </div>

                <div className="row mt-3">
                  <div className="col-6">
                    <label htmlFor="customCategory">
                      Sélectionner une catégorie personnalisée
                    </label>
                    <select
                      name="customCategory"
                      value={productData.customCategory}
                      onChange={handleChange}
                      id="customCategory"
                      className="form-control"
                    >
                      <option value="" defaultValue disabled hidden>
                        Choisir ici
                      </option>
                      {customCategoriesMap()}
                    </select>
                  </div>
                  <div className="col-6"></div>
                </div>

                <div className="row">
                  <div className="col-6">
                    <input
                      className="form-control mt-2"
                      type="file"
                      name="images"
                      onChange={handleChange}
                    />
                  </div>
                  <div className="col-6"></div>
                </div>

                <button className="btn btn-dark mt-2">Ajouter produit</button>
              </form>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
