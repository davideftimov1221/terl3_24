import { Outlet, useOutletContext } from "react-router-dom";
import "../../../../../styles/Create.css";

export default function Create() {
  const [
    auth,
    customCategories,
    onDelete,
    setClickCategory,
    setCategory,
    handleClickUpdateCategory,
  ] = useOutletContext();
  return (
    <>
      <Outlet
        context={[
          auth,
          customCategories,
          onDelete,
          setClickCategory,
          setCategory,
          handleClickUpdateCategory,
        ]}
      />
    </>
  );
}
