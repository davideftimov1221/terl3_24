import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import AddCustomCategories from "../../../../../components/AddCustomCategories";
import "../../../../../styles/Create.css";

const ADDCUSTOMCATEGORY_URL =
  "http://localhost:6969/api/store/customCategory/create";
const UDPATECUSTOMCATEGORY_URL =
  "http://localhost:6969/api/store/customCategory/update";
const DELETECUSTOMCATEGORY_URL =
  "http://localhost:6969/api/store/customCategory/delete/";

export default function CustomCategory() {
  const [
    auth,
    clickCategory,
    setClickCategory,
    customCategory,
    setCustomCategory,
  ] = useOutletContext();

  const [customCategories, setCustomCategories] = useState([]);

  function funSetCustomCategories(customcategory) {
    if (clickCategory.clickAddCategory) {
      const customcategories = customCategories.slice();
      customcategories.push(customcategory);
      setCustomCategories(customcategories);
    } else if (clickCategory.clickUpdateCategory) {
      const customcategories = customCategories.slice();
      const index = customcategories.findIndex(
        (element) => element.id === customcategory.id
      );
      customcategories[index] = customcategory;
      setCustomCategories(customcategories);
    } else if (clickCategory.clickDeleteCategory) {
      const customcategories = customCategories.slice();
      const index = customcategories.findIndex(
        (element) => element.id === customcategory.id
      );
      customcategories.splice(index, 1);
      setCustomCategories(customcategories);
    }
  }

  useEffect(() => {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const response = async () =>
      await fetch(
        `http://localhost:6969/api/store/customCategory/all/${auth.id}`,
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          //console.log(result);
          const data = JSON.parse(result);
          setCustomCategories(data.result);
        })
        .catch((error) => console.log("error", error));
    response();
  }, []);

  useEffect(() => {
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(customCategory);

    let customcategory;
    if (clickCategory.clickAddCategory) {
      const requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      const response = async () => {
        await fetch(ADDCUSTOMCATEGORY_URL, requestOptions)
          .then((response) => response.text())
          .then((result) => {
            //console.log(result);
            customcategory = JSON.parse(result);
          })
          .catch((error) => console.log("error", error));
        funSetCustomCategories(customcategory);
        setClickCategory("add");
      };
      response();
    } else if (clickCategory.clickUpdateCategory) {
      const requestOptions = {
        method: "PUT",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      const response = async () => {
        await fetch(UDPATECUSTOMCATEGORY_URL, requestOptions)
          .then((response) => response.text())
          .then((result) => {
            //console.log(result);
            customcategory = JSON.parse(result);
          })
          .catch((error) => console.log("error", error));
        funSetCustomCategories(customcategory);
        setClickCategory("update");
      };
      response();
    } else if (clickCategory.clickDeleteCategory) {
      const requestOptions = {
        method: "DELETE",
        headers: myHeaders,
        redirect: "follow",
      };
      const response = async () => {
        await fetch(
          DELETECUSTOMCATEGORY_URL + customCategory.id,
          requestOptions
        )
          .then((response) => response.text())
          .then((result) => {
            /*console.log(result)*/
          })
          .catch((error) => console.log("error", error));
        funSetCustomCategories(customCategory);
        setClickCategory("delete");
      };
      response();
    }
  }, [clickCategory]);

  return (
    <>
      <div className="container d-flex flex-column align-items-center">
        <button
          type="button"
          name=""
          id=""
          className="mt-5  btn btn-outline-dark btn-lg btn-block"
          onClick={() => {
            setCustomCategory((prevData) => ({
              ...prevData,
              id: 0,
              name: "",
              provider: auth.id,
              valid: "add",
            }));
          }}
        >
          Add Category +
        </button>
        <AddCustomCategories
          auth={auth}
          customCategories={customCategories}
          setCustomCategory={setCustomCategory}
        />
      </div>
    </>
  );
}
