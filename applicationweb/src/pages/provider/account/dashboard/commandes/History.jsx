import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import Orders from "../../../../../components/Orders";

export default function History() {
  const [auth] = useOutletContext();
  const [historyOrder, setHistoryOrder] = useState();

  function deleteOrder(idOrder) {
    const historyorder = historyOrder.slice();
    const index = historyorder.findIndex((order) => order.id === idOrder);
    historyorder.splice(index, 1);
    setHistoryOrder(historyorder);
  }

  function rows() {
    let productDivList;

    if (historyOrder) {
      productDivList = historyOrder.map((order) => {
        return (
          <Orders order={order} deleteOrder={deleteOrder} key={order.id} />
        );
      });
    }
    return productDivList;
  }

  useEffect(() => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    // const order = {
    //   id: auth.id,
    //   date: "DESC",
    //   step: "CONFIRMATION_ORDER",
    // };
    // const url = new URL(
    //   `http://localhost:6969/api/order/current-provider-orders`
    // );
    // url.search = new URLSearchParams(order);
    ////console.log(url);

    const responseNewOrder = async () =>
      await fetch(
        `http://localhost:6969/api/order/current-provider-past-orders?id=${auth.id}`,
        requestOptions
      )
        .then((response) => response.json())
        .then((result) => {
          //console.log(result.result);
          let historyorder = [];
          for (const order of result.result) {
            if (order.orderState.pickedUp || order.orderState.received) {
              historyorder.push(order);
            }
          }
          //console.log(historyorder);
          setHistoryOrder(result.result);
        })
        .catch((error) => console.log("error", error));
    responseNewOrder();
  }, []);

  return <>{rows()}</>;
}
