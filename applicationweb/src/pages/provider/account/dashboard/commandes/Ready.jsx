import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import Orders from "../../../../../components/Orders";

export default function Ready() {
  const [auth] = useOutletContext();
  const [readyOrder, setReadyOrder] = useState();

  function deleteOrder(idOrder) {
    const acceptorder = readyOrder.slice();
    const index = acceptorder.findIndex((order) => order.id === idOrder);
    acceptorder.splice(index, 1);
    setReadyOrder(acceptorder);
  }

  function rows() {
    let productDivList;

    if (readyOrder) {
      productDivList = readyOrder.map((order) => {
        return (
          <Orders order={order} deleteOrder={deleteOrder} key={order.id} />
        );
      });
    }
    return productDivList;
  }

  useEffect(() => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const order = {
      id: auth.id,
      date: "DESC",
      step: "READY_ORDER",
    };
    const url = new URL(
      `http://localhost:6969/api/order/current-provider-orders`
    );
    url.search = new URLSearchParams(order);
    //console.log(url);

    const responseNewOrder = async () =>
      await fetch(url, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          //console.log(result.result);
          setReadyOrder(result.result);
        })
        .catch((error) => console.log("error", error));
    responseNewOrder();
  }, []);

  return <>{rows()}</>;
}
