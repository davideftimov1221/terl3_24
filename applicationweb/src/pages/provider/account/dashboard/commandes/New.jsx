import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import Orders from "../../../../../components/Orders";

export default function New() {
  const [auth] = useOutletContext();
  const [newOrder, setNewOrder] = useState();

  function deleteOrder(idOrder) {
    const neworder = newOrder.slice();
    const index = neworder.findIndex((order) => order.id === idOrder);
    neworder.splice(index, 1);
    setNewOrder(neworder);
  }

  function rows() {
    let productDivList;

    if (newOrder) {
      productDivList = newOrder.map((order) => {
        return (
          <Orders order={order} deleteOrder={deleteOrder} key={order.id} />
        );
      });
    }
    return productDivList;
  }

  useEffect(() => {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const order = {
      id: auth.id,
      date: "ASC",
      step: "NEW_ORDER",
    };
    const url = new URL(
      `http://localhost:6969/api/order/current-provider-orders`
    );
    url.search = new URLSearchParams(order);
    //console.log(url);

    const responseNewOrder = async () =>
      await fetch(url, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          //console.log(result.result);
          setNewOrder(result.result);
        })
        .catch((error) => console.log("error", error));
    responseNewOrder();
  }, []);

  return <>{rows()}</>;
}
