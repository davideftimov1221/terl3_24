import { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import Orders from "../../../../../components/Orders";

export default function Confirm() {
  const [auth] = useOutletContext();
  const [confirmOrder, setConfirmOrder] = useState();

  function deleteOrder(idOrder) {
    const confirmorder = confirmOrder.slice();
    const index = confirmorder.findIndex((order) => order.id === idOrder);
    confirmorder.splice(index, 1);
    setConfirmOrder(confirmorder);
  }

  function rows() {
    let productDivList;

    if (confirmOrder) {
      productDivList = confirmOrder.map((order) => {
        return (
          <Orders order={order} deleteOrder={deleteOrder} key={order.id} />
        );
      });
    }
    return productDivList;
  }

  useEffect(() => {
    var requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const order = {
      id: auth.id,
      date: "DESC",
      step: "CONFIRMATION_ORDER",
    };
    const url = new URL(
      `http://localhost:6969/api/order/current-provider-orders`
    );
    url.search = new URLSearchParams(order);
    //console.log(url);

    const responseNewOrder = async () =>
      await fetch(url, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          //console.log(result.result);
          let confirmorder = [];
          for (const order of result.result) {
            if (!order.orderState.received) {
              confirmorder.push(order);
            }
          }
          //console.log(confirmorder);
          setConfirmOrder(confirmorder);
        })
        .catch((error) => console.log("error", error));
    responseNewOrder();
  }, []);

  return <>{rows()}</>;
}
