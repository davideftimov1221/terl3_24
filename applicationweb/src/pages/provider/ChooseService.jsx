import { Navigate, useNavigate } from "react-router-dom";
import React, { useState, useContext } from "react";
import AuthContext from "../../context/AuthProvider";

export default function ChooseService() {
  const { auth } = useContext(AuthContext);
  const [registerData, setRegisterData] = useState({ cService: "" });

  const navigate = useNavigate();

  function handleChange(event) {
    setRegisterData((prevRegisterData) => ({
      ...prevRegisterData,
      [event.target.name]: event.target.value,
    }));
  }

  function handleSumbit(event) {
    event.preventDefault();
    //console.log(registerData);
    if (registerData.cService === "store") {
      navigate("../createstore");
    }
  }

  return (
    <>
      {!auth.token ? (
        <Navigate to="../login" />
      ) : (
        <div className="container mt-5 cv">
          <h2 className="text-center">Choose one service</h2>
          <div className="row mt-5">
            <div className="col-6 text-center">
              <input
                type="radio"
                id="service"
                name="cService"
                value="service"
                className="btn-check"
                checked={registerData.cService === "service"}
                onChange={handleChange}
              />
              <label className="btn-lg btn-outline-dark" htmlFor="service">
                Service
              </label>
            </div>
            <div className="col-6 text-center">
              <input
                type="radio"
                id="store"
                name="cService"
                value="store"
                className="btn-check"
                checked={registerData.cService === "store"}
                onChange={handleChange}
              />
              <label className="btn-lg btn-outline-dark" htmlFor="store">
                Store
              </label>
            </div>
          </div>

          <div className="row">
            <div className="col-12 text-center">
              <input
                className="btn-lg btn-dark"
                onClick={handleSumbit}
                type="submit"
                value="Next"
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
}
