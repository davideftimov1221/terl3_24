import { Navigate, useNavigate } from "react-router-dom";
import React, { useState, useContext } from "react";
import AuthContext from "../../context/AuthProvider";
import "../../styles/Policy.css";

export default function ChooseService() {
  const { auth } = useContext(AuthContext);
  const [policyData, setPolicyData] = useState({
    delivery: "",
    selfPickUpOption: "SELF_PICK_UP",
    validDuration: "",
    providerId: auth.id,
  });

  const navigate = useNavigate();

  function handleChange(event) {
    setPolicyData((prevPolicyData) => ({
      ...prevPolicyData,
      [event.target.name]: event.target.value,
    }));
  }

  async function handleSumbit(event) {
    event.preventDefault();

    await fetch("http://localhost:6969/api/policy/create", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(policyData),
    }).then((result) => {
      //console.log("New user added");
      navigate("../activityfield/category");
    });
  }

  //console.log(policyData);
  return (
    <>
      {!auth.token ? (
        <Navigate to="../login" />
      ) : (
        <div className="container mt-5 cv">
          <h2 className="text-center">Create policy</h2>
          <div className="row">
            <div className="col-4"></div>
            <div className="col-4">
              <form
                onSubmit={handleSumbit}
                className="register-form d-flex flex-column justify-content-center align-items-center"
              >
                <h6>Commande avec livraison?</h6>
                <div className="d-flex flex-row policyRadio">
                  <input
                    type="radio"
                    id="yes"
                    name="delivery"
                    value={true}
                    className="mt-1 me-1"
                    checked={policyData.delivery === "true"}
                    onChange={handleChange}
                  />
                  <label htmlFor="simpleUser">Yes</label>

                  <input
                    type="radio"
                    id="no"
                    name="delivery"
                    value={false}
                    className="mt-1 ms-3 me-1"
                    checked={policyData.delivery === "false"}
                    onChange={handleChange}
                  />
                  <label htmlFor="provider">No</label>
                </div>
                <input
                  type="text"
                  placeholder="Pickup heures de validation"
                  onChange={handleChange}
                  name="validDuration"
                  value={policyData.validDuration}
                  className="form-control mt-2"
                />

                <button className="btn btn-dark btn-lg mt-2">Register</button>
              </form>
            </div>
            <div className="col-4"></div>
          </div>
        </div>
      )}
    </>
  );
}
