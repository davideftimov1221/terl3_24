import "../styles/home2.css";
import Head from "../components/Head";
import Footer from "../components/Footer";
import ProductItem from "../components/ProductItem";
import { useContext, useEffect, useState } from "react";
import AuthContext from "../context/AuthProvider";
import NoUserHome from "../components/NoUserHome";

export default function Home() {
  const { auth } = useContext(AuthContext);
  const [products, setProducts] = useState();

  function defaultHome() {
    return <NoUserHome />;
  }
  useEffect(() => {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    if (auth.token && auth.userType === "simpleUser") {
      const response = async () =>
        await fetch(
          `http://localhost:6969/api/product/interest?page=1&id=${auth.id}`,
          requestOptions
        )
          .then((response) => response.text())
          .then((result) => {
            //console.log(result);
            const prod = JSON.parse(result);
            setProducts(prod.result);
          })
          .catch((error) => console.log("error", error));
      response();
    }
  }, []);

  //useEffect(() => console.log(products), [products]);
  return (
    <>
      <div className="content">
        <Head />
        <section id="products">
          <div className="container-fluid mt-4 px-0" id="homeProducts">
            {auth.token && auth.userType === "simpleUser" ? (
              <>
                <h3 className="mt-3 ms-3">Pour toi</h3>
                <div className="row productCardRow">
                  {products &&
                    products.map((product) => (
                      <ProductItem
                        key={product.id}
                        Product={product}
                        authId={auth.id}
                      />
                    ))}
                </div>
              </>
            ) : (
              defaultHome()
            )}
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
