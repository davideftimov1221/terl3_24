import React, { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import axios from "../../api/axios";
import InProgressOrder from "../../components/InProgressOrder";

export default function InProgress() {
  const [auth] = useOutletContext();

  /* GET ALL ORDERS IN PROGRESS */

  const [inProgressList, setInProgressList] = useState();

  /* DELETE ORDER IN INPROGRESSLIST AT ORDERID*/
  function deleteOrderInProgressList(orderId) {
    const inprogresslist = inProgressList.slice();
    const index = inProgressList.findIndex((order) => order.id === orderId);
    inprogresslist.splice(index, 1);
    setInProgressList(inprogresslist);
  }
  useEffect(() => {
    const getOrdersInProgress = async () => {
      const response = await axios.get(
        `http://localhost:6969/api/order/current-user/inProgress?id=${auth.id}&date=ASC`
      );
      //console.log(response.data.result);
      let inprogresslist = [];
      for (const order of response.data.result) {
        if (!order.orderState.pickedUp) {
          inprogresslist.push(order);
        }
      }
      //console.log(inprogresslist);
      setInProgressList(response.data.result);
    };
    getOrdersInProgress();
  }, []);
  //console.log(inProgressList)

  /* GET ALL ORDERS IN PROGRESS */

  /*RENDER ORDERS IN PROGRESS */

  function rows() {
    let offerDivList;
    if (inProgressList) {
      offerDivList = inProgressList.map((inProgressOrder) => {
        return (
          <InProgressOrder
            key={inProgressOrder.id}
            id={inProgressOrder.id}
            date={inProgressOrder.createAt}
            orderState={inProgressOrder.orderState}
            orderType={inProgressOrder.orderType}
            storeName={inProgressOrder.storeName}
            storeAddress={inProgressOrder.storeAddress}
            bill={inProgressOrder.bill}
            image={inProgressOrder.imageStore}
            deleteOrder={deleteOrderInProgressList}
          />
        );
      });
    }
    return offerDivList;
  }

  /*RENDER ORDERS IN PROGRESS */

  return (
    <div className="container">
      <div className="row">{rows()}</div>
    </div>
  );
}
