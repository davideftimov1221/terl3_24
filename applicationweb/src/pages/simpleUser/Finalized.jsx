import React, { useEffect, useState } from "react";
import { useOutletContext } from "react-router-dom";
import InProgressOrder from "../../components/InProgressOrder";

export default function Finalized() {
  const [auth] = useOutletContext();
  const [finalizedOrder, setFinalizedOrder] = useState();

  useEffect(() => {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };

    const responseNewOrder = async () =>
      await fetch(
        `http://localhost:6969/api/order/current-user/finalized?id=${auth.id}&date=ASC`,
        requestOptions
      )
        .then((response) => response.json())
        .then((result) => {
          //console.log(result.result);
          setFinalizedOrder(result.result);
        })
        .catch((error) => console.log("error", error));
    responseNewOrder();
  }, []);

  /*RENDER ORDERS IN PROGRESS */

  function rows() {
    let offerDivList;
    if (finalizedOrder) {
      offerDivList = finalizedOrder.map((order) => {
        return (
          <InProgressOrder
            key={order.id}
            id={order.id}
            date={order.createAt}
            orderState={order.orderState}
            orderType={order.orderType}
            storeName={order.storeName}
            storeAddress={order.storeAddress}
            bill={order.bill}
            image={order.imageStore}
          />
        );
      });
    }
    return offerDivList;
  }

  /*RENDER ORDERS IN PROGRESS */

  return (
    <div className="container">
      <div className="row">{rows()}</div>
    </div>
  );
}
