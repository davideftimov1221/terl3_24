import { useContext, useEffect, useState } from "react";
import "../../styles/CreateStore.css";
import "react-phone-number-input/style.css";
import AuthContext from "../../context/AuthProvider";
import { Navigate, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationCrosshairs } from "@fortawesome/free-solid-svg-icons";

export default function CreateUser() {
  const { auth } = useContext(AuthContext);

  const [userData, setUserData] = useState({
    userId: auth.id,
    firstName: "",
    lastName: "",
    birthDay: "",
  });
  const [address, setAddress] = useState({});
  const [textAddress, setTextAddress] = useState("");
  const [city, setCity] = useState();

  const navigate = useNavigate();

  async function handleChange(event) {
    if (event.currentTarget.name === "Address") {
      setTextAddress(event.target.value);

      if (textAddress.length > 15) {
        let requestOptions = {
          method: "GET",
          redirect: "follow",
        };
        let geodata;
        await fetch(
          `http://api.positionstack.com/v1/forward?access_key=e722c14b6990095206adfd4d3678242c&query=${event.target.value}`,
          requestOptions
        )
          .then((response) => response.json())
          .then((result) => {
            geodata = result;
            //console.log(geodata);
          })
          .catch((error) => console.log("error", error));

        geodata = geodata.data.filter((data) => data.number !== null);

        if (geodata[0]) {
          let add = geodata[0];
          add = {
            streetNumber: add.number,
            admin: add.region,
            subAdmin: add.administrative_area,
            locality: add.locality,
            streetName: add.street,
            postalCode: add.postal_code,
            countryCode: add.country_code,
            coutryName: add.country,
            latitude: add.latitude,
            longitude: add.longitude,
            fullAddress: add.label,
            userId: auth.id,
          };

          setAddress(add);
          //console.log(add);
          let cit = geodata[0];

          cit = {
            latitude: cit.latitude,
            longitude: cit.longitude,
            name: cit.name,
            displayName: cit.name,
            country: cit.country,
            userId: auth.id,
          };
          setCity(cit);
        }
      }
    } else {
      setUserData((prevStoreData) => ({
        ...prevStoreData,
        [event.target.name]: event.target.value,
      }));
    }
  }

  async function handleGeoPosition() {
    const success = async (position) => {
      const latitude = position.coords.latitude;
      const longitude = position.coords.longitude;
      //console.log(latitude + " " + longitude);

      let requestOptions = {
        method: "GET",
        redirect: "follow",
      };
      let geodata;
      await fetch(
        `http://api.positionstack.com/v1/reverse?access_key=e722c14b6990095206adfd4d3678242c&query=${latitude},${longitude}&limit=3`,
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          geodata = JSON.parse(result);
          //console.log(geodata);
        })
        .catch((error) => console.log("error", error));

      geodata = geodata.data.filter((data) => data.number !== null);

      if (geodata[0]) {
        let add = geodata[0];
        add = {
          streetNumber: add.number,
          admin: add.region,
          subAdmin: add.administrative_area,
          locality: add.locality,
          streetName: add.street,
          postalCode: add.postal_code,
          countryCode: add.country_code,
          coutryName: add.country,
          latitude: add.latitude,
          longitude: add.longitude,
          fullAddress: add.label,
          userId: auth.id,
        };

        setAddress(add);
        setTextAddress(add.fullAddress);
        let cit = geodata[0];

        cit = {
          latitude: cit.latitude,
          longitude: cit.longitude,
          name: cit.name,
          displayName: cit.name,
          country: cit.country,
          userId: auth.id,
        };
        setCity(cit);

        //console.log(geodata);
      }
    };
    const error = () => {};

    navigator.geolocation.getCurrentPosition(success, error);
  }
  async function handleSubmit(event) {
    event.preventDefault();

    const headerUserData = new Headers();
    headerUserData.append("Content-Type", "application/json");

    const userdata = JSON.stringify(userData);

    const requestUserData = {
      method: "POST",
      headers: headerUserData,
      body: userdata,
      redirect: "follow",
    };

    await fetch("http://localhost:6969/api/user/Information", requestUserData)
      .then((response) => response.text())
      .then((result) => {
        /*console.log(result)*/
      })
      .catch((error) => console.log("error", error));

    const headerAddress = new Headers();
    headerAddress.append("Content-Type", "application/json");

    const add = JSON.stringify(address);

    const requestAddress = {
      method: "POST",
      headers: headerAddress,
      body: add,
      redirect: "follow",
    };

    await fetch("http://localhost:6969/api/address/create", requestAddress)
      .then((response) => response.text())
      .then((result) => {
        /*console.log(result)*/
      })
      .catch((error) => console.log("error", error));

    var headerCity = new Headers();
    headerCity.append("Content-Type", "application/json");

    var cit = JSON.stringify(city);

    var requestCity = {
      method: "POST",
      headers: headerCity,
      body: cit,
      redirect: "follow",
    };

    await fetch("http://localhost:6969/api/user/default-city", requestCity)
      .then((response) => response.text())
      .then((result) => {
        /*console.log(result)*/
      })
      .catch((error) => console.log("error", error));
    //console.log(userData);

    navigate("../activityfield/category");
  }
  useEffect(() => {
    const requestOptions = {
      method: "GET",
      redirect: "follow",
    };
    let error;
    const response = async () =>
      await fetch(
        `http://localhost:6969/api/user/Information/${auth.id}`,
        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          //console.log(result);
          error = JSON.parse(result);
          //console.log(error);
          if (error.lastName) {
            //console.log("coucou");
            navigate("/");
          }
        })
        .catch((error) => console.log("error", error));
    response();
  }, []);
  return (
    <>
      {!auth.token ? (
        <Navigate to="../login" />
      ) : (
        <div className="container createstore">
          <h1 className="createstore-title">Your Informations</h1>
          <form
            onSubmit={handleSubmit}
            className="d-flex flex-column justify-content-center align-items-center"
          >
            <div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control border-0 border-bottom createstore-text-enter"
                  name="firstName"
                  value={userData.firstName}
                  placeholder="First Name"
                  onChange={handleChange}
                />
              </div>
              <div className="form-group">
                <input
                  type="text"
                  className="form-control border-0 border-bottom createstore-text-enter"
                  name="lastName"
                  value={userData.lastName}
                  placeholder="Last Name"
                  onChange={handleChange}
                />
              </div>
              <div className="form-group d-flex align-items-center">
                <input
                  type="text"
                  className="form-control border-0 border-bottom createstore-text-enter"
                  list="suggestion"
                  name="Address"
                  value={textAddress}
                  placeholder="Address"
                  onChange={handleChange}
                />
                <datalist id="suggestion">
                  <option value={address.fullAddress} />
                </datalist>
                <FontAwesomeIcon
                  onClick={handleGeoPosition}
                  className=""
                  size="2x"
                  icon={faLocationCrosshairs}
                />
              </div>
              <div className="form-group">
                <input
                  type="date"
                  className="form-control border-0 border-bottom createstore-text-enter"
                  name="birthDay"
                  max="2005-12-31"
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="mt-5">
              <button className="btn btn-dark createstore-button-next">
                Next
              </button>
            </div>
          </form>
        </div>
      )}
    </>
  );
}
