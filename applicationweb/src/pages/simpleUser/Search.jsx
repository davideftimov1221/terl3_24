import { useContext, useEffect, useState } from "react";
import AuthContext from "../../context/AuthProvider";
import axios from "../../api/axios";
import ProductContext from "../../context/ProductProvider";

import Head from "../../components/Head";
import Footer from "../../components/Footer";
import SearchStore from "../../components/search/SearchStore";

export default function Search() {
  const { searchGlobalData } = useContext(ProductContext);
  const { auth } = useContext(AuthContext);
  //storeId = Number(storeId);
  const [storeAroundIdList, setStoreAroundIdList] = useState();
  const [searchData, setSearchData] = useState({
    searchText: searchGlobalData.searchText,
    radius: searchGlobalData.radius,
    category: searchGlobalData.category,
    latitude: "",
    longitude: "",
  });
  //console.log(searchGlobalData);

  useEffect(() => {
    const getUserAddress = async () => {
      const response = await axios.get(
        `http://localhost:6969/api/address/${auth.id}`
      );
      //console.log(response.data.result);
      setSearchData((prevAddressData) => ({
        ...prevAddressData,
        latitude: response.data.result[0].latitude,
        longitude: response.data.result[0].longitude,
      }));
    };
    getUserAddress();
  }, []);

  useEffect(() => {
    if (searchData.latitude !== "") {
      const getStoresAround = async () => {
        const response = await axios.get(
          `http://localhost:6969/api/store/store-around?distance=${searchData.radius}&longitude=${searchData.longitude}&latitude=${searchData.latitude}&category=${searchData.category}`
        );
        //console.log(response.data.result);
        setStoreAroundIdList(
          response.data.result.map((store) => {
            return store.id;
          })
        );
      };
      getStoresAround();
    }
  }, [searchData]);

  //console.log(searchData)
  //console.log(storeAroundIdList)

  function rows() {
    let productList;
    if (storeAroundIdList) {
      productList = storeAroundIdList.map((store) => {
        return (
          <SearchStore
            key={store}
            id={store}
            searchText={searchGlobalData.searchText}
          />
        );
      });
    }
    return productList;
  }

  return (
    <>
      <div className="content">
        <Head />
        <section id="products">
          <div className="container-fluid mt-3 px-0">
            <div className="row productCardRow">{rows()}</div>
          </div>
        </section>
      </div>
      <Footer />
    </>
  );
}
