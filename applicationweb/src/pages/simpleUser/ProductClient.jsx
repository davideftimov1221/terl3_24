import "../../styles/Connect.css";
import Head from "../../components/Head";
import Footer from "../../components/Footer";
import "../../styles/ProductClient.css";

import { useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import ProductContext from "../../context/ProductProvider";
import AuthContext from "../../context/AuthProvider";
import axios from "../../api/axios";

export default function ProductClient() {
  const { product } = useContext(ProductContext);
  const { auth } = useContext(AuthContext);
  const [quantity, setQuantity] = useState(1);
  const navigate = useNavigate();
  const productVariant = product.productVariants[0];

  function funSetQuantity(count) {
    if (count >= 1 && count <= productVariant.unit) {
      setQuantity(count);
    }
  }

  function setPrix() {
    if (productVariant.offer === null) {
      return productVariant.price;
    } else {
      if (productVariant.offer.type === "FIXED") {
        return productVariant.offer.newPrice;
      } else {
        return productVariant.price * (1 - 1 / productVariant.offer.percentage);
      }
    }
  }
  function addToCart() {
    const variantId = product.productVariants[0].id;
    //console.log(`localhost:6969/api/cart/add?userId=${auth.id}&variantId=${variantId}&quantity=${quantity}`);
    axios({
      url: `http://localhost:6969/api/cart/add?userId=${auth.id}&variantId=${variantId}&quantity=${quantity}`,
      method: "POST",
    })
      .then(() => {
        //console.log("Added to cart");
        navigate("../");
      })
      .catch((error) => console.log("error", error));
  }

  //useEffect(() => console.log(product), []);
  return (
    <>
      <div className="content">
        <Head />
        <div className="container-fluid mt-2">
          <div className="row">
            <div className="col-8">
              <img
                src={
                  "http://localhost:6969/api/product/image/" +
                  product.images[0].image
                }
                alt=""
                className="d-block w-100 productSlidePhotos"
              />
            </div>
            <div className="col-4">
              <h1>{product.name}</h1>
              <hr />
              <div className="">
                <h5>
                  <u>Description</u>
                </h5>
                <p>{product.description}</p>
              </div>
              <hr />
              <h5>
                <u>Prix</u>
              </h5>
              <p>
                {quantity}x{setPrix()}€
              </p>
              <p>Total : {quantity * setPrix()}€</p>
              <hr />
              <h5>
                <u>Quantité</u>
              </h5>
              <div className="d-flex">
                <button
                  onClick={() => funSetQuantity(quantity - 1)}
                  id="quantityBtnMinus"
                  className="btn btn-outline-dark"
                >
                  –
                </button>
                <div className="">
                  <p id="quantityCount" className="mx-2">
                    {quantity}
                  </p>
                </div>
                <button
                  onClick={() => funSetQuantity(quantity + 1)}
                  id="quantityBtnPlus"
                  className="btn btn-outline-dark"
                >
                  +
                </button>
              </div>
              <hr />
              <div className="d-flex justify-content-center">
                <button onClick={addToCart} id="" className="btn btn-dark">
                  Ajouter au panier
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
