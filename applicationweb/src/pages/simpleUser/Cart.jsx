import "../../styles/Cart.css";
import Head from "../../components/Head";
import Footer from "../../components/Footer";

import AuthContext from "../../context/AuthProvider";
import React, { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import axios from "axios";
import CartStore from "../../components/CartStore";

export default function Cart() {
  const { auth } = useContext(AuthContext);
  const [cartInfo, setCartInfo] = React.useState();
  const [deleteAlert, setDeleteAlert] = React.useState(true);

  useEffect(() => {
    const getCartInfo = async () => {
      const response = await axios.get(
        "http://localhost:6969/api/cart/" + auth.id
      );
      setCartInfo(response.data.cartProductVariants);
      //console.log(response.data.cartProductVariants);
    };
    getCartInfo();
  }, [deleteAlert]);

  //cartInfo &&//console.log(cartInfo[2].storeName);

  //console.log(stores);

  function mappingStores() {
    let stores = [];
    if (cartInfo) {
      for (let i = 0; i < cartInfo.length; i++) {
        if (!stores.includes(cartInfo[i].storeName)) {
          stores.push(cartInfo[i].storeName);
        }
      }
      //console.log(stores)
      let cartStore = [];
      for (let i = 0; i < stores.length; i++) {
        const index = cartInfo.findIndex(
          (element) => stores[i] === element.storeName
        );
        const key = cartInfo[index].storeId;
        cartStore.push(
          <CartStore
            name={stores[i]}
            cartInfo={cartInfo}
            authId={auth.id}
            deleteAlert={deleteAlert}
            setDeleteAlert={setDeleteAlert}
            key={key}
          />
        );
      }

      return cartStore;
    }
  }

  return (
    <>
      {!(auth.userType === "simpleUser") ? (
        <Navigate to="../login" />
      ) : (
        <>
          <div className="content">
            <Head />
            <div className="container mt-5">
              <h3 className="mb-5">Mon panier</h3>
              {mappingStores()}
            </div>
          </div>
          <Footer />
        </>
      )}
    </>
  );
}
