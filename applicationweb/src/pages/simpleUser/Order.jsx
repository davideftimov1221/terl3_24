import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import AuthContext from "../../context/AuthProvider";

import Head from "../../components/Head";
import Footer from "../../components/Footer";

export default function Order() {
  let { storeId } = useParams();
  storeId = Number(storeId);
  const { auth } = useContext(AuthContext);
  const [orderUser, setOrderUser] = useState({
    userId: auth.id,
    storeId: storeId,
    orderType: "DELIVERY",
    address: null,
    cartProductVariantIds: [],
    receiverFirstName: "",
    receiverLastName: "",
    receiverBirthDay: "",
  });
  const [addressUser, setAddressUser] = useState();
  const [checked, setChecked] = useState(true);
  function funSetChecked() {
    checked ? setChecked(false) : setChecked(true);
  }
  const navigate = useNavigate();
  function handleChange(event) {
    if (event.target.value === "null") {
      setOrderUser((prevData) => ({ ...prevData, address: null }));
    } else {
      const index = addressUser.findIndex(
        (address) => address.id === Number(event.target.value)
      );
      //console.log(event.target.value);
      const address = addressUser[index];
      //console.log(address);
      setOrderUser((prevData) => ({ ...prevData, address: address }));
    }
  }
  function funSetOrderType(event) {
    if (event.target.checked) {
      setOrderUser((prevData) => ({
        ...prevData,
        orderType: event.target.name,
      }));
      funSetChecked();
    }
  }
  async function handleSubmit(event) {
    event.preventDefault();
    const myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify(orderUser);

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };
    if (
      (orderUser.orderType === "DELIVERY" && orderUser.address !== null) ||
      orderUser.orderType === "SELFPICKUP"
    ) {
      await fetch("http://localhost:6969/api/order/create", requestOptions)
        .then((response) => response.text())
        .then((result) => {
          //console.log(result);
          navigate("../../");
        })
        .catch((error) => console.log("error", error));
    }
  }
  useEffect(() => {
    var requestAddressOptions = {
      method: "GET",
      redirect: "follow",
    };
    const responseAddress = async () =>
      await fetch(
        `http://localhost:6969/api/address/${auth.id}`,
        requestAddressOptions
      )
        .then((response) => response.json())
        .then((result) => {
          //console.log(result);
          setAddressUser(result.result);
          //console.log(result.result);
        })
        .catch((error) => console.log("error", error));
    responseAddress();

    var requestCartOptions = {
      method: "GET",
      redirect: "follow",
    };

    const responseCart = async () =>
      await fetch(
        `http://localhost:6969/api/cart/${auth.id}`,
        requestCartOptions
      )
        .then((response) => response.json())
        .then((result) => {
          //console.log(result.cartProductVariants);
          const productsCart = [];
          for (let i = 0; i < result.cartProductVariants.length; i++) {
            const product = result.cartProductVariants[i];

            if (product.storeId === storeId) {
              productsCart.push(product.id);
            }
          }
          //console.log(productsCart);
          setOrderUser((prevData) => ({
            ...prevData,
            cartProductVariantIds: productsCart,
          }));
        })
        .catch((error) => console.log("error", error));
    responseCart();

    var requestInfoUserOptions = {
      method: "GET",
      redirect: "follow",
    };

    const responseInfoUser = async () =>
      await fetch(
        `http://localhost:6969/api/user/Information/${auth.id}`,
        requestInfoUserOptions
      )
        .then((response) => response.json())
        .then((result) => {
          //console.log(result);
          setOrderUser((prevData) => ({
            ...prevData,
            receiverFirstName: result.firstName,
            receiverLastName: result.lastName,
            receiverBirthDay: result.birthDay,
          }));
        })
        .catch((error) => console.log("error", error));
    responseInfoUser();
  }, []);

  //useEffect(() => console.log(orderUser), [orderUser]);

  return (
    <>
      <div className="content">
        <Head />
        <div className="container ">
          <form
            onSubmit={handleSubmit}
            className="d-flex flex-column justify-content-center align-items-center"
          >
            <div className="btn-group mt-5 mb-3" data-toggle="buttons">
              <input
                className="checkbox btn-check"
                type="checkbox"
                name="DELIVERY"
                id="Delivery"
                checked={checked}
                onChange={funSetOrderType}
              />
              <label
                htmlFor="Delivery"
                className="btn btn-outline-dark btn-lg btn-block shadow-none"
              >
                Delivery
              </label>
              <input
                className="checkbox btn-check"
                name="SELFPICKUP"
                type="checkbox"
                id="SelfPickup"
                checked={!checked}
                onChange={funSetOrderType}
              />
              <label
                className="btn btn-outline-dark btn-lg btn-block shadow-none"
                htmlFor="SelfPickup"
              >
                SelfPickup
              </label>
            </div>
            {checked && (
              <div className="mb-3">
                <h5>
                  Choisir Adresse de Livraison :
                  <select onChange={handleChange} className="form-control">
                    <option value="null">. . .</option>
                    {addressUser &&
                      addressUser.map((address) => (
                        <option key={address.id} value={address.id}>
                          {address.fullAddress}
                        </option>
                      ))}
                  </select>
                </h5>
              </div>
            )}
            <button
              type="submit"
              className="btn btn-outline-dark btn-lg btn-block"
            >
              Confirm Order
            </button>
          </form>
        </div>
      </div>
      <Footer />
    </>
  );
}
