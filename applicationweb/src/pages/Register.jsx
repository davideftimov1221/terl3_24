import "../styles/Register.css";
import Head from "../components/Head";
import Footer from "../components/Footer";
import React, { useState } from "react";

import { useNavigate } from "react-router-dom";

export default function Register() {
  const [registerData, setRegisterData] = useState({
    email: "",
    userName: "",
    passWord: "",
    passWord2: "",
    userType: "",
  });

  function handleChange(event) {
    setRegisterData((prevRegisterData) => ({
      ...prevRegisterData,
      [event.target.name]: event.target.value,
    }));
  }

  const navigate = useNavigate();

  function handleSumbit(event) {
    event.preventDefault();
    //console.log(registerData);
    if (registerData.userType === "simpleUser") {
      fetch("http://localhost:6969/api/user/register", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(registerData),
      }).then(() => {
        //console.log("New user added");
        navigate("/");
      });
    } else {
      fetch("http://localhost:6969/api/provider/register", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(registerData),
      }).then(() => {
        //console.log("New provider added");
        navigate("/");
      });
    }
  }
  return (
    <>
      <div className="content">
        <Head />
        <div className="container-fluid registerCnt">
          <div className="row">
            <div className="col-0 col-md-6" id="registerLeft"></div>
            <div className="col-sm-12 col-md-6" id="registerRight">
              <div className="container">
                <h1 className="login-title my-5">Register</h1>
                <form
                  onSubmit={handleSumbit}
                  className="register-form d-flex flex-column"
                >
                  <input
                    type="email"
                    placeholder="Email"
                    onChange={handleChange}
                    name="email"
                    value={registerData.email}
                    className="mt-2 form-control"
                  />

                  <input
                    type="text"
                    placeholder="Username"
                    onChange={handleChange}
                    name="userName"
                    value={registerData.userName}
                    className="mt-2 form-control"
                  />

                  <input
                    type="password"
                    placeholder="Password"
                    onChange={handleChange}
                    name="passWord"
                    value={registerData.passWord}
                    className="mt-2 form-control"
                  />

                  <input
                    type="password"
                    placeholder="Confirm Password"
                    onChange={handleChange}
                    name="passWord2"
                    value={registerData.passWord2}
                    className="mt-2 form-control"
                  />

                  <div className="d-flex flex-row my-2 registerRadio">
                    <input
                      type="radio"
                      id="simpleUser"
                      name="userType"
                      value="simpleUser"
                      className="mt-1 me-1"
                      checked={registerData.userType === "simpleUser"}
                      onChange={handleChange}
                    />
                    <label htmlFor="simpleUser">Client</label>

                    <input
                      type="radio"
                      id="provider"
                      name="userType"
                      value="provider"
                      className="mt-1 ms-3 me-1"
                      checked={registerData.userType === "provider"}
                      onChange={handleChange}
                    />
                    <label htmlFor="provider">Fournisseur</label>
                  </div>
                  <button className="btn btn-dark mt-2 rgFormBtns">
                    Register
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
