import Head from "../components/Head";
import Footer from "../components/Footer";
import React from "react";
import { useState, useContext } from "react";
import AuthContext from "../context/AuthProvider";
import { useNavigate } from "react-router-dom";
import "../styles/Login.css";

import axios from "../api/axios";
const USERLOGIN_URL = "/api/user/login";
const PROVIDERLOGIN_URL = "/api/provider/login";
const STOREINFORMATION_URL = "/api/store/Information/";
const USERINFORMATION_URL = "/api/user/Information/";
export default function Login() {
  //--------------------//
  //State de AuthContext//
  //--------------------//
  const { auth, setAuth } = useContext(AuthContext);

  //-----------------------------------------------------//
  //State des données d'authentification de l'utilisateur//
  //-----------------------------------------------------//
  const [loginData, setLoginData] = useState({
    email: "",
    passWord: "",
    userType: "",
  });

  //--------------------------------------//
  //State de messages d'erreurs des Fetchs//
  //--------------------------------------//
  const [errMsg, setErrMsg] = useState("");

  //-----------------------------------------//
  //Met à jour le State login sur un OnChange//
  //-----------------------------------------//
  function handleChange(event) {
    setLoginData((prevLoginData) => ({
      ...prevLoginData,
      [event.target.name]: event.target.value,
    }));
  }
  //------------------//
  //Hook de navigation//
  //------------------//
  const navigate = useNavigate();

  //-----------------------------------------------------------------//
  //Fonction qui envoie les données présentes dans le state loginData//
  //-----------------------------------------------------------------//
  const handleSubmit = async (event) => {
    event.preventDefault();
    setErrMsg("");
    //console.log(loginData);
    if (loginData.userType === "simpleUser") {
      let id;
      try {
        //--------------------------------------------------------//
        //Envoie des données au back-end pour une connexion client//
        //--------------------------------------------------------//
        const response = await axios.post(
          USERLOGIN_URL,
          JSON.stringify({
            email: loginData.email,
            passWord: loginData.passWord,
          }),
          {
            headers: { "Content-Type": "application/json" },
          }
        );
        //console.log(JSON.stringify(response?.data));
        //----------------------------------------------------------------------------------//
        //Enregistrement des données Client retournées par le back-end dans un State Context//
        //----------------------------------------------------------------------------------//
        const token = response?.data?.token;
        const id = response?.data?.id;
        const email = loginData.email;
        const passWord = loginData.passWord;
        const userType = loginData.userType;
        setAuth({ email, id, passWord, userType, token });

        //---------------------------------------------------------------------------------//
        //Demande de donnée au back-end pour savoir si le client à déjà remplit ses données//
        //---------------------------------------------------------------------------------//
        const requestOptions = {
          method: "GET",
          redirect: "follow",
        };
        let error;
        await fetch(
          `http://localhost:6969/api/user/Information/${id}`,
          requestOptions
        )
          .then((response) => response.text())
          .then((result) => {
            //console.log(result);
            error = JSON.parse(result);
            if (error.firstName) {
              //console.log("coucou");
              navigate("../");
            } else {
              navigate("../createuser");
            }
          })
          .catch((error) => console.log("error", error));
      } catch (err) {
        if (!err?.response) {
          setErrMsg("Aucune réponse du serveur");
        } else if (err.response?.status === 401) {
          setErrMsg("Nom d'utilisateur ou mot de passe manquant");
        } else {
          setErrMsg("Échec de la connexion");
        }
      }

      try {
        //-----------------------------------------------------------------------------------//
        //Demande de donnée au back-end pour savoir si le provider à déjà remplit ses données//
        //-----------------------------------------------------------------------------------//
        const response = await axios.get(USERINFORMATION_URL + id, {
          headers: { "Content-Type": "application/json" },
        });
        //console.log(response?.data);
        if (response.data?.userId === null) {
          navigate("../createuser");
        } else {
          navigate("/");
        }
        //console.log(response?.data?.userId);
      } catch (err) {
        if (err.response?.status === 404) {
          navigate("../createuser");
        }
      }
    } else if (loginData.userType === "provider") {
      let id;
      //----------------------------------------------------------//
      //Envoie des données au back-end pour une connexion provider//
      //----------------------------------------------------------//
      try {
        const response = await axios.post(
          PROVIDERLOGIN_URL,
          JSON.stringify({
            email: loginData.email,
            passWord: loginData.passWord,
          }),
          {
            headers: { "Content-Type": "application/json" },
          }
        );
        //console.log(response?.data);

        //------------------------------------------------------------------------------------//
        //Enregistrement des données Provider retournées par le back-end dans un State Context//
        //------------------------------------------------------------------------------------//
        if (response?.data) {
          const token = response.data?.token;
          id = response.data?.id;
          const email = loginData.email;
          const userType = loginData.userType;
          setAuth({ id, email, userType, token });
        }
      } catch (err) {
        if (!err?.response) {
          setErrMsg("Aucune réponse du serveur");
        } else if (err.response?.status === 401) {
          setErrMsg("Nom d'utilisateur ou mot de passe manquant");
        } else {
          setErrMsg("Échec de la connexion");
        }
      }
      let error;
      try {
        //-----------------------------------------------------------------------------------//
        //Demande de donnée au back-end pour savoir si le provider à déjà remplit ses données//
        //-----------------------------------------------------------------------------------//
        const response = await axios.get(STOREINFORMATION_URL + id, {
          headers: { "Content-Type": "application/json" },
        });
        //console.log(response?.data);
      } catch (err) {
        if (err.response?.status === 404) {
          error = 404;
          navigate("../chooseservice");
        }
      }
      navigate("../chooseservice");

      if (error !== 404) {
        navigate("/account/dashboard");
      }
    }
  };

  function navigateRegister() {
    navigate("../register");
  }
  //console.log(errMsg);
  return (
    <>
      <div className="content">
        <Head />
        <div className="container-fluid loginCnt">
          <div className="row">
            <div className="col-0 col-md-6" id="loginLeft"></div>
            <div className="col-sm-12 col-md-6" id="loginRight">
              <div className="container">
                <h1 className="login-title my-5">Login</h1>
                <p className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
                <form
                  onSubmit={handleSubmit}
                  className="login-form d-flex flex-column align-items-start"
                >
                  {/*----------//
                  //Input Email//
                  //----------*/}
                  <input
                    type="email"
                    placeholder="Email"
                    onChange={handleChange}
                    name="email"
                    value={loginData.email}
                    className="mt-2 form-control"
                  />
                  {/*-------------//
                  //Input PassWord//
                  //-------------*/}
                  <input
                    type="password"
                    placeholder="Password"
                    onChange={handleChange}
                    name="passWord"
                    value={loginData.passWord}
                    className="mt-2 form-control"
                  />
                  {/*----------------------------------//
                  //Input Radio pour choisir simpleUser//
                  //----------------------------------*/}
                  <div className="d-flex flex-row my-2 loginRadio">
                    <input
                      type="radio"
                      id="simpleUser"
                      name="userType"
                      value="simpleUser"
                      className="mt-1 me-1"
                      checked={loginData.userType === "simpleUser"}
                      onChange={handleChange}
                    />

                    <label htmlFor="simpleUser">Client</label>
                    {/*--------------------------------//
                  //Input Radio pour choisir Provider//
                  //--------------------------------*/}
                    <input
                      type="radio"
                      id="provider"
                      name="userType"
                      value="provider"
                      className="mt-1 ms-3 me-1"
                      checked={loginData.userType === "provider"}
                      onChange={handleChange}
                    />
                    <label htmlFor="provider">Fournisseur</label>
                  </div>
                  {/*-----------//
                  //Button Login//
                  //-----------*/}
                  <button className="btn btn-dark mt-2 lgnFormBtns">
                    Login
                  </button>
                </form>
                <h3 className="mt-5">Pas encore inscrit?</h3>
                <button
                  className="btn btn-dark mb-5 lgnFormBtns"
                  onClick={navigateRegister}
                >
                  Register
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
}
